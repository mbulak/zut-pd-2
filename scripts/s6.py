#!/usr/bin/env python3
# coding=utf-8

from math import floor
from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import re

fig = plt.figure()

pattern = re.compile("[pcn]+_\\d+_M_\\d+"
                     "_(?P<train_gc>\d+)"
                     "_(?P<alt_cnt_min>\d+)"
                     "_(?P<alt_cnt_max>\d+)")


def parse_alt_cnts(filename):
    m = pattern.match(filename)
    if m:
        alt_cnt_min = int(m.group("alt_cnt_min"))
        alt_cnt_max = int(m.group("alt_cnt_max"))
        return alt_cnt_min, alt_cnt_max
    else:
        raise ValueError(f"{filename} didn't match {pattern.pattern}")


def gen_xticks(alt_cnt_min, alt_cnt_max, cnt):
    cnt -= 1
    xtick_step = int(max(floor((alt_cnt_max - alt_cnt_min)) / cnt, 1))
    return [alt_cnt_min, *[x for x in range(
        alt_cnt_min + xtick_step, alt_cnt_max - xtick_step + 1, xtick_step
    )], alt_cnt_max]


def gen_mean(filename):
    alt_cnt_min, alt_cnt_max = parse_alt_cnts(filename)
    outfilename = f"{filename}_mean"
    df = pd.read_csv(f"csv/{filename}.txt", header=None)

    axis = fig.subplots(nrows=1, ncols=2, sharex=True, sharey=True)

    axis[0].set_title("Wybór najlepszej")
    axis[1].set_title("Wybór najgorszej")
    fig.text(0.5625, 0.02, "liczba alternatyw", ha="center", va="center")
    axis[0].set_ylabel("poprawność")

    xs = range(alt_cnt_min, alt_cnt_max + 1)
    yss = [[] for _ in range(6)]
    df.mean(axis=0)
    for x in xs:
        df_x = df.loc[df[0] == x]
        df_x_mean = df_x.mean(axis=0)
        for i in range(0, 6):
            yss[i].append(df_x_mean[i + 1])

    xticks = gen_xticks(alt_cnt_min, alt_cnt_max, 5)

    axis[0].scatter(xs, yss[0], c="k", marker="+", label="AHP")
    axis[0].scatter(xs, yss[1], c="k", marker="x", label="COMET")
    axis[0].scatter(xs, yss[2], facecolor="none", edgecolor="k", label="TOPSIS")
    axis[0].set_xticks(xticks)
    axis[0].yaxis.set_major_formatter(FormatStrFormatter("%.2f"))
    axis[0].legend(loc=3)
    axis[1].scatter(xs, yss[3], c="k", marker="+", label="AHP")
    axis[1].scatter(xs, yss[4], c="k", marker="x", label="COMET")
    axis[1].scatter(xs, yss[5], facecolor="none", edgecolor="k", label="TOPSIS")
    axis[1].set_xticks(xticks)

    fig.subplots_adjust(top=0.95, right=0.99, bottom=0.1, left=0.1, hspace=0, wspace=0)
    plt.savefig(f"plots/{outfilename}.pdf")
    plt.clf()
    print(f"{outfilename} done.")


if __name__ == '__main__':
    gen_mean("ppp_9_M_100000_5_3_30")
    gen_mean("ccc_9_M_100000_5_3_30")
    gen_mean("nnn_9_M_100000_5_3_30")
    gen_mean("pcn_9_M_100000_5_3_30")
    gen_mean("pppp_9_M_100000_5_3_30")
