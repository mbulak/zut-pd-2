#!/usr/bin/env python3
# coding=utf-8

from matplotlib.ticker import FormatStrFormatter
from s6 import parse_alt_cnts, gen_xticks
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

fig = plt.figure()
kwargs = {"c": "k", "alpha": 1 / 16}


def gen_swarm(filename):
    alt_cnt_min, alt_cnt_max = parse_alt_cnts(filename)
    outfilename = f"{filename}_swarm"

    print("reading...", end="", flush=True)
    df = pd.read_csv(f"csv/{filename}.txt", header=None)

    axis = fig.subplots(nrows=1, ncols=3, sharex=True, sharey=True)

    axis[0].set_title("AHP")
    axis[1].set_title("COMET")
    axis[2].set_title("TOPSIS")
    fig.text(0.5625, 0.02, "liczba alternatyw", ha="center", va="center")
    axis[0].set_ylabel("poprawność")

    xs = range(alt_cnt_min, alt_cnt_max + 1)
    alt_cnt = alt_cnt_max - alt_cnt_min + 1
    ahp_arr = np.empty((100, alt_cnt))
    comet_arr = np.empty((100, alt_cnt))
    topsis_arr = np.empty((100, alt_cnt))

    df.mean(axis=0)
    for x in xs:
        df_x = df.loc[df[0] == x]
        for i in range(0, 100):
            df_slice_mean = df_x.iloc[i * 1000 + 1: (i + 1) * 1000 + 1].mean(axis=0)
            ahp_arr[i, x - 3] = df_slice_mean[1]
            comet_arr[i, x - 3] = df_slice_mean[2]
            topsis_arr[i, x - 3] = df_slice_mean[3]

    for i in range(0, 100):
        axis[0].plot(xs, ahp_arr[i], **kwargs)
        axis[1].plot(xs, comet_arr[i], **kwargs)
        axis[2].plot(xs, topsis_arr[i], **kwargs)
        print(f"\r{i + 1}/100", end="", flush=True)

    xticks = gen_xticks(alt_cnt_min, alt_cnt_max, 4)

    axis[0].set_xticks(xticks)
    axis[0].yaxis.set_major_formatter(FormatStrFormatter("%.2f"))
    axis[1].set_xticks(xticks)
    axis[2].set_xticks(xticks)

    fig.subplots_adjust(top=0.95, right=0.99, bottom=0.1, left=0.1, hspace=0, wspace=0)
    print("\rsaving...", end="", flush=True)
    plt.savefig(f"plots/{outfilename}.png", dpi=150)
    plt.clf()
    print(f"\r{outfilename} done.")


if __name__ == '__main__':
    gen_swarm("ppp_9_M_100000_5_3_30")
    gen_swarm("ccc_9_M_100000_5_3_30")
    gen_swarm("nnn_9_M_100000_5_3_30")
    gen_swarm("pcn_9_M_100000_5_3_30")
    gen_swarm("pppp_9_M_100000_5_3_30")
