#!/usr/bin/env python3
# coding=utf-8

import matplotlib.pyplot as plt
import pandas as pd

fig = plt.figure()


def gen_hist(prefix, left_part, right_part, suffix):
    left_filename = f"{prefix}_{left_part}_{suffix}"
    right_filename = f"{prefix}_{right_part}_{suffix}"
    outfilename = f"{prefix}_{left_part}_{right_part}_{suffix}_hist"
    df_l = pd.read_csv(f"csv/{left_filename}.txt", header=None)
    df_r = pd.read_csv(f"csv/{right_filename}.txt", header=None)

    axis = fig.subplots(nrows=1, ncols=2, sharex=True, sharey=True)

    axis[0].set_title("Zestaw A")
    axis[1].set_title("Zestaw B")
    fig.text(0.5625, 0.02, "nieliniowość FRM", ha="center", va="center")
    axis[0].set_ylabel("liczba modeli")

    axis[0].hist(df_l[0], bins=20, ec="k", lw="1", fc="#ffffff")
    axis[1].hist(df_r[0], bins=20, ec="k", lw="1", fc="#ffffff")

    fig.subplots_adjust(top=0.95, right=0.99, bottom=0.1, left=0.125, hspace=0, wspace=0)
    plt.savefig(f"plots/{outfilename}.pdf")
    plt.clf()
    print(f"{outfilename} done.")


gen_hist("ppp_9", "L_100000", "R_10000_10", "5_4")
gen_hist("ccc_9", "L_100000", "R_10000_10", "5_4")
gen_hist("nnn_9", "L_100000", "R_10000_10", "5_4")
gen_hist("pcn_9", "L_100000", "R_10000_10", "5_4")
gen_hist("pppp_9", "L_100000", "R_10000_10", "5_4")
