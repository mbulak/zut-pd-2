#!/usr/bin/env python3
# coding=utf-8

from mcdm import FRM, COMET, TOPSIS, AHP
import numpy as np
import os.path

np.set_printoptions(suppress=True)


def gen_csv(frm_args, frm_cnt=100_000, overwrite=False, train_gc=5, alt_cnt_min=3, alt_cnt_max=30):
    def gen_filename():
        ms = frm_args["ms"]
        gc = frm_args["gc"]
        if not ms or not gc:
            raise ValueError()
        return f"""{"".join(ms)}_{gc}_M_{frm_cnt}_{train_gc}_{alt_cnt_min}_{alt_cnt_max}"""

    filename = gen_filename()
    csv_path = f"csv/{filename}.txt"
    if not overwrite and os.path.exists(csv_path):
        print(f"{filename} exists. Skipping.")
        return
    csv = open(csv_path, "w")

    iterations = frm_cnt * (alt_cnt_max - alt_cnt_min + 1)
    progress = 0
    for i in range(0, frm_cnt):
        frm = FRM(**frm_args)
        frm_dim = frm.dim()
        linear_coefs = frm.linear_coefs()
        grid_train = [np.linspace(0.0, 1.0, train_gc) for _ in range(0, frm_dim)]
        y_train = frm.invoke_grid(grid_train)
        comet = COMET.from_grid_y(grid_train, y_train)
        for j in range(alt_cnt_min, alt_cnt_max + 1):
            x_test = np.random.rand(j, frm_dim)
            y_test = frm.invoke_array2(x_test)
            frm_min_idx = np.argmin(y_test)
            frm_max_idx = np.argmax(y_test)

            ahp_result = AHP.invoke_linear_coefs(x_test, linear_coefs)
            comet_result = comet.invoke_array2(x_test)
            topsis_result = TOPSIS.invoke_linear_coefs(x_test, linear_coefs, "M4")

            csv.write(
                f"{j},"
                f"{int(frm_min_idx == np.argmin(ahp_result))   },"
                f"{int(frm_min_idx == np.argmin(comet_result)) },"
                f"{int(frm_min_idx == np.argmin(topsis_result))},"
                f"{int(frm_max_idx == np.argmax(ahp_result))   },"
                f"{int(frm_max_idx == np.argmax(comet_result)) },"
                f"{int(frm_max_idx == np.argmax(topsis_result))}"
                "\n"
            )
            progress += 1
            print(f"\r{progress}/{iterations}", end="", flush=True)
    print(f"\r{filename} done with {iterations} iterations.")
    csv.close()


gen_csv(frm_args={"ms": ["p"] * 3, "gc": 9})
gen_csv(frm_args={"ms": ["c"] * 3, "gc": 9})
gen_csv(frm_args={"ms": ["n"] * 3, "gc": 9})
gen_csv(frm_args={"ms": ["p", "c", "n"], "gc": 9})
gen_csv(frm_args={"ms": ["p"] * 4, "gc": 9})

print("done")
