#!/usr/bin/env python3

import matplotlib.pyplot as plt

fig = plt.figure(figsize=(8, 2))
ax = fig.add_subplot(111)
ax.plot([0.0, 0.5], [1.0, 0.0], c="k")
ax.plot([0.0, 0.5, 1.0], [0.0, 1.0, 0.0], c="k")
ax.plot([0.5, 1.0], [0.0, 1.0], c="k")
ax.plot([0.0, 0.0], [0.0, 1.0], c="k", ls="--")
ax.plot([1.0, 1.0], [0.0, 1.0], c="k", ls="--")

ax.set_xlim(-0.05, 1.05)
ax.set_ylim(0.0, 1.15)

ax.set_xticks([0.0, 0.25, 0.5, 0.75, 1.0])
ax.set_yticks([0.0, 0.25, 0.5, 0.75, 1.0])

ax.text(0.0, 1.04, "$C_1$", ha="center")
ax.text(0.5, 1.04, "$C_2$", ha="center")
ax.text(1.0, 1.04, "$C_3$", ha="center")

ax.grid(b=True, alpha=0.4)

# plt.show()

plt.savefig("plots/comet_tns.pdf")
