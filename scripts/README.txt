s1
  generuje dane używane przez pozostałe skrypty
  format csv
  współczynnik_nieliniowości modelu frm
  współczynnik spearmana dla rang     zwrócownych przez ahp, comet, topsis
  współczynnik spearmana dla kendalla zwrócownych przez ahp, comet, topsis
  jeden wątek dlatego generowanie zajmuje 4h(?)
  można każdą linikę gen_csv... możnaby uruchomić równolegle
s2
  generuje wszystkie tabele użyte w pracy na podstawie danych z s1
s3
  generuje histogramy użyte w pracy na podstawie danych z s1
s4
  generuje wykresy typu scatter użyte w pracy na podstawie danych z s1
s5
  generuje dane używane przez pozostałę skrypty
  format csv
  liczba alternatyw
  czy ahp, comet, topsis poprawnie przewidziało najgorszą alternatywę (1-prawda, 0-falsz)
  czy ahp, comet, topsis poprawnie przewidziało najgorszą alternatywę (1-prawda, 0-falsz)
s6
  generuje wykresy typu scatter użyte w pracy na podstawie danych z s5 (druga seria wykresów scatter)
s7
  generuje siostrzane wykresy do tych z s6 na podstawie danych z s5
s8
  generuje wykresy do rozdziału 2.1
s9
  generuje wykres do rozdziału 1.2
