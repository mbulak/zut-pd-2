#!/usr/bin/env python3
# coding=utf-8

from mcdm import FRM, COMET, TOPSIS, AHP, spearman, kendall
import mcdm
import numpy as np
import os
import os.path

np.set_printoptions(suppress=True)

def calc_then_save(file, frm, grid_train, grid_test):
    linear_coefs, nonlinearity = frm.linear_coefs_nonlinearity()

    y_train = frm.invoke_grid(grid_train)
    x_test = mcdm.grid_to_mesh(grid_test)

    comet = COMET.from_grid_y(grid_train, y_train)

    frm_ranks = frm.rank_grid_avg(grid_test)
    ahp_ranks = AHP.rank_linear_coefs_avg(x_test, linear_coefs)
    comet_ranks = comet.rank_grid_avg(grid_test)
    topsis_ranks = TOPSIS.rank_linear_coefs_avg(x_test, linear_coefs, normalization="M4")

    file.write(
        f"{frm.nonlinearity()              },"
        f"{spearman(frm_ranks, ahp_ranks   )},"
        f"{spearman(frm_ranks, comet_ranks )},"
        f"{spearman(frm_ranks, topsis_ranks)},"
        f"{kendall (frm_ranks, ahp_ranks   )},"
        f"{kendall (frm_ranks, comet_ranks )},"
        f"{kendall (frm_ranks, topsis_ranks)}"
        "\n"
    )


def gen_csv_lin(frm_args, frm_cnt=100_000, train_gc=5, test_gc=4, overwrite=False):
    def gen_filename():
        ms = frm_args["ms"]
        gc = frm_args["gc"]
        if not ms or not gc:
            raise ValueError()
        return f"""{"".join(ms)}_{gc}_L_{frm_cnt}_{train_gc}_{test_gc}"""

    def gen_granules(gc):
        return np.linspace(0.0, 1.0, gc)

    filename = gen_filename()
    csv_path = f"csv/{filename}.txt"
    if not overwrite and os.path.exists(csv_path):
        print(f"{filename} exists. Skipping.")
        return
    csv = open(csv_path, "w")

    for i in range(0, frm_cnt):
        frm = FRM(**frm_args)
        frm_shape = frm.shape()

        grid_train = [gen_granules(train_gc) for _ in frm_shape]
        grid_test = [gen_granules(test_gc) for _ in frm_shape]

        calc_then_save(csv, frm, grid_train, grid_test)

        print(f"\r{i + 1}/{frm_cnt}", end="", flush=True)
    csv.close()
    print(f"\r{filename} done with {frm_cnt} iterations.")


def gen_csv_rnd(frm_args, frm_cnt=10_000, test_cnt=10, train_gc=5, test_gc=4, overwrite=False):
    def gen_filename():
        ms = frm_args["ms"]
        gc = frm_args["gc"]
        if not ms or not gc:
            raise ValueError()
        return f"""{"".join(ms)}_{gc}_R_{frm_cnt}_{test_cnt}_{train_gc}_{test_gc}"""

    def gen_granules(gc):
        r = np.random.rand(gc - 2)
        r.sort()
        arr = np.empty(gc)
        arr[0] = 0.0
        arr[gc - 1] = 1.0
        arr[1:(gc - 1)] = r
        return arr

    filename = gen_filename()
    csv_path = f"csv/{filename}.txt"
    if not overwrite and os.path.exists(csv_path):
        print(f"{filename} exists. Skipping.")
        return
    csv = open(csv_path, "w")

    iterations = frm_cnt * test_cnt
    for i in range(0, frm_cnt):
        frm = FRM(**frm_args)
        frm_shape = frm.shape()
        for j in range(0, test_cnt):
            grid_train = [gen_granules(train_gc) for _ in frm_shape]
            grid_test = [gen_granules(test_gc) for _ in frm_shape]

            calc_then_save(csv, frm, grid_train, grid_test)

            print(f"\r{i * test_cnt + j + 1}/{iterations}", end="", flush=True)
    print(f"\r{filename} done with {iterations} iterations.")
    csv.close()


gen_csv_lin(frm_args={"ms": ["p"] * 3, "gc": 9})
gen_csv_lin(frm_args={"ms": ["c"] * 3, "gc": 9})
gen_csv_lin(frm_args={"ms": ["n"] * 3, "gc": 9})
gen_csv_lin(frm_args={"ms": ["p", "c", "n"], "gc": 9})
gen_csv_lin(frm_args={"ms": ["p"] * 4, "gc": 9})

gen_csv_lin(frm_args={"ms": ["p"] * 3, "gc": 9}, train_gc=2)
gen_csv_lin(frm_args={"ms": ["c"] * 3, "gc": 9}, train_gc=2)
gen_csv_lin(frm_args={"ms": ["n"] * 3, "gc": 9}, train_gc=2)
gen_csv_lin(frm_args={"ms": ["p", "c", "n"], "gc": 9}, train_gc=2)
gen_csv_lin(frm_args={"ms": ["p"] * 4, "gc": 9}, train_gc=2)

gen_csv_lin(frm_args={"ms": ["p"] * 3, "gc": 9}, train_gc=7)
gen_csv_lin(frm_args={"ms": ["c"] * 3, "gc": 9}, train_gc=7)
gen_csv_lin(frm_args={"ms": ["n"] * 3, "gc": 9}, train_gc=7)
gen_csv_lin(frm_args={"ms": ["p", "c", "n"], "gc": 9}, train_gc=7)
gen_csv_lin(frm_args={"ms": ["p"] * 4, "gc": 9}, train_gc=7)

gen_csv_rnd(frm_args={"ms": ["p"] * 3, "gc": 9})
gen_csv_rnd(frm_args={"ms": ["c"] * 3, "gc": 9})
gen_csv_rnd(frm_args={"ms": ["n"] * 3, "gc": 9})
gen_csv_rnd(frm_args={"ms": ["p", "c", "n"], "gc": 9})
gen_csv_rnd(frm_args={"ms": ["p"] * 4, "gc": 9})

gen_csv_rnd(frm_args={"ms": ["p"] * 3, "gc": 9}, train_gc=2)
gen_csv_rnd(frm_args={"ms": ["c"] * 3, "gc": 9}, train_gc=2)
gen_csv_rnd(frm_args={"ms": ["n"] * 3, "gc": 9}, train_gc=2)
gen_csv_rnd(frm_args={"ms": ["p", "c", "n"], "gc": 9}, train_gc=2)
gen_csv_rnd(frm_args={"ms": ["p"] * 4, "gc": 9}, train_gc=2)

gen_csv_rnd(frm_args={"ms": ["p"] * 3, "gc": 9}, train_gc=7)
gen_csv_rnd(frm_args={"ms": ["c"] * 3, "gc": 9}, train_gc=7)
gen_csv_rnd(frm_args={"ms": ["n"] * 3, "gc": 9}, train_gc=7)
gen_csv_rnd(frm_args={"ms": ["p", "c", "n"], "gc": 9}, train_gc=7)
gen_csv_rnd(frm_args={"ms": ["p"] * 4, "gc": 9}, train_gc=7)

print("done")
