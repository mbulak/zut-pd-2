#!/usr/bin/env python3
# coding=utf-8

import pandas as pd

sub_template = r"""
\parbox[t]{2mm}{\multirow{4}{*}{\rotatebox[origin=c]{90}{%s}}}
& \large{\(\min  {\mu}\)} & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\
& \large{\(\max  {\mu}\)} & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\
& \large{\(\bar  {\mu}\)} & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\
& \large{\(\tilde{\mu}\)} & %.4f & %.4f & %.4f & %.4f & %.4f & %.4f \\
\hline
"""

template = r"""
\begin{table}[H]
\begin{center}
\caption[Tabela]{Właściwości wygenerowanych FRM.}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
\multicolumn{2}{|c|}{} & \multicolumn{3}{c|}{\bf Zestaw A} & \multicolumn{3}{c|}{\bf Zestaw B} \\
\cline{3-8}
\multicolumn{2}{|c|}{} & {\bf AHP} & {\bf COMET} & {\bf TOPSIS} & {\bf AHP} & {\bf COMET} & {\bf TOPSIS} \\
\hline
""" + sub_template * 2 + r"""
\end{tabular}
\label{table:%s}
\end{center}
\end{table}
"""


def gen_table(prefix, left_part, right_part, suffix):
    left_filename = f"{prefix}_{left_part}_{suffix}"
    right_filename = f"{prefix}_{right_part}_{suffix}"
    outfilename = f"{prefix}_{left_part}_{right_part}_{suffix}_table"
    df_l = pd.read_csv(f"csv/{left_filename}.txt", header=None)
    df_r = pd.read_csv(f"csv/{right_filename}.txt", header=None)
    min_l, min_r = df_l.min(axis=0), df_r.min(axis=0)
    max_l, max_r = df_l.max(axis=0), df_r.max(axis=0)
    mean_l, mean_r = df_l.mean(axis=0), df_r.mean(axis=0)
    median_l, median_r = df_l.median(axis=0), df_r.median(axis=0)

    with open(f"tables/{outfilename}.tex", "w+") as f:
        f.write(template % (
            # r"\(\rho\) Spearmana",
            r"\(\rho\)",
            *min_l[1:4], *min_r[1:4],
            *max_l[1:4], *max_r[1:4],
            *mean_l[1:4], *mean_r[1:4],
            *median_l[1:4], *median_r[1:4],
            # r"\(\tau\) Kendalla",
            r"\(\tau\)",
            *min_l[4:7], *min_r[4:7],
            *max_l[4:7], *max_r[4:7],
            *mean_l[4:7], *mean_r[4:7],
            *median_l[4:7], *median_r[4:7],
            f"{outfilename}",
        ))
    print(f"{outfilename} done.")


gen_table("ppp_9", "L_100000", "R_10000_10", "5_4")
gen_table("ccc_9", "L_100000", "R_10000_10", "5_4")
gen_table("nnn_9", "L_100000", "R_10000_10", "5_4")
gen_table("pcn_9", "L_100000", "R_10000_10", "5_4")
gen_table("pppp_9", "L_100000", "R_10000_10", "5_4")
