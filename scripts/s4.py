#!/usr/bin/env python3
# coding=utf-8

from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
import pandas as pd

kwargs = {"s": 20, "marker": ".", "c": "k", "alpha": 1 / 16}
# kwargs = {"s": 20, "marker": ".", "alpha": 1 / 16, "facecolor":"none", "edgecolor":"k"}
fig = plt.figure()


def gen_scatter(prefix, left_part, right_part, suffix):
    left_filename = f"{prefix}_{left_part}_{suffix}"
    right_filename = f"{prefix}_{right_part}_{suffix}"
    outfilename = f"{prefix}_{left_part}_{right_part}_{suffix}_scatter"
    df_l = pd.read_csv(f"csv/{left_filename}.txt", header=None)
    df_r = pd.read_csv(f"csv/{right_filename}.txt", header=None)

    axis = fig.subplots(nrows=6, ncols=2, sharex=True, sharey=True)

    fig.text(0.565, 0.02, "nieliniowość FRM", ha="center", va="center")
    fig.text(0.019, 0.72, "wartość ρ Spearmana", ha="center", va="center", rotation="vertical")
    fig.text(0.019, 0.28, "wartość τ Kendalla", ha="center", va="center", rotation="vertical")
    axis[0, 0].set_title("Zestaw A")
    axis[0, 1].set_title("Zestaw B")

    ylabels = ["AHP", "COMET", "TOPSIS"]

    for i in range(0, 6):
        axis[i, 0].set_ylabel(ylabels[i % 3])
        axis[i, 0].scatter(df_l[0], df_l[i + 1], **kwargs)
        axis[i, 0].yaxis.set_major_formatter(FormatStrFormatter("%.2f"))
        print(f"\r{(i+1) * 2}/12", end="", flush=True)
        axis[i, 1].scatter(df_r[0], df_r[i + 1], **kwargs)
        print(f"\r{(i+1) * 2 + 1}/12", end="", flush=True)

    fig.subplots_adjust(top=0.95, right=0.99, bottom=0.1, left=0.14, hspace=0, wspace=0)
    print("\rsaving...", end="", flush=True)
    fig.savefig(f"plots/{outfilename}.png", dpi=150)
    plt.clf()
    print(f"\r{outfilename} done.")


gen_scatter("ppp_9", "L_100000", "R_10000_10", "5_4")
gen_scatter("ccc_9", "L_100000", "R_10000_10", "5_4")
gen_scatter("nnn_9", "L_100000", "R_10000_10", "5_4")
gen_scatter("pcn_9", "L_100000", "R_10000_10", "5_4")
gen_scatter("pppp_9", "L_100000", "R_10000_10", "5_4")

gen_scatter("ppp_9", "L_100000", "R_10000_10", "2_4")
gen_scatter("ccc_9", "L_100000", "R_10000_10", "2_4")
gen_scatter("nnn_9", "L_100000", "R_10000_10", "2_4")
gen_scatter("pcn_9", "L_100000", "R_10000_10", "2_4")
gen_scatter("pppp_9", "L_100000", "R_10000_10", "2_4")

gen_scatter("ppp_9", "L_100000", "R_10000_10", "7_4")
gen_scatter("ccc_9", "L_100000", "R_10000_10", "7_4")
gen_scatter("nnn_9", "L_100000", "R_10000_10", "7_4")
gen_scatter("pcn_9", "L_100000", "R_10000_10", "7_4")
gen_scatter("pppp_9", "L_100000", "R_10000_10", "7_4")
