#![allow(dead_code)]
#![feature(test)]
#![feature(specialization)]
#![feature(type_ascription)]

extern crate test;

mod linalg;
mod mcdm;
mod py;
mod stat;

use crate::mcdm::{ahp, comet, frm, topsis, Rank, Rules};
use ndarray::{prelude::ArrayView1, Array1, Array2};
use numpy::{IntoPyArray, PyArrayDyn};
use pyo3::{
    exceptions,
    prelude::{
        pyclass, pyfunction, pymethods, pymodule, ObjectProtocol, Py, PyErr, PyModule, PyResult,
        Python,
    },
    types::{PyDict, PyList, PyString, PyTuple},
    wrap_pyfunction, PyObject, PyRawObject,
};
use std::convert::TryFrom;

macro_rules! value_error {
    ($msg: expr) => {
        PyErr::new::<exceptions::ValueError, _>($msg)
    };
}
macro_rules! py_array_dyn_to_av1 {
    ($pad: expr) => {
        if let &[len] = $pad.shape() {
            Ok($pad.reshape(len)?.as_array())
        } else {
            Err(value_error!(format!(
                "Oczekiwano tablicy 1-wymiarowej dla parametru {}. Otrzymano {}-wymiarową.",
                stringify!($pad),
                $pad.ndim()
            )))
        }
    };
}
macro_rules! py_array_dyn_to_av2 {
    ($pad: expr) => {
        if let &[rows, cols] = $pad.shape() {
            Ok($pad.reshape((rows, cols))?.as_array())
        } else {
            Err(value_error!(format!(
                "Oczekiwano tablicy 2-wymiarowej dla parametru {}. Otrzymano {}-wymiarową.",
                stringify!($pad),
                $pad.ndim()
            )))
        }
    };
}
macro_rules! expect_none {
    ($v: ident) => {
        if $v.is_some() {
            return Err(value_error!(format!(
                "`{}` powtarza się w argumentach.",
                stringify!($v)
            )));
        }
    };
}

fn pylist_to_av1s(grid: &PyList) -> Result<Vec<ArrayView1<f64>>, PyErr> {
    grid.iter()
        .map(|obj| {
            obj.extract::<&PyArrayDyn<f64>>()
                .map_err(|_| value_error!("Oczekiwano listy tablic numpy."))
                .and_then(|py_arr| {
                    if let &[len] = py_arr.shape() {
                        Ok(py_arr.reshape(len)?.as_array())
                    } else {
                        Err(value_error!("Oczekiwano listy 1-wymiarowych tablic numpy."))
                    }
                })
        })
        .collect::<Result<Vec<_>, _>>()
}

#[pyfunction]
fn linear_coefs(
    x: &PyArrayDyn<f64>,
    y: &PyArrayDyn<f64>,
    py: Python,
) -> PyResult<Py<PyArrayDyn<f64>>> {
    Ok(linalg::linear_coefs_from_x_y_arrays(
        &py_array_dyn_to_av2!(x)?,
        &py_array_dyn_to_av1!(y)?,
        true,
    )?
    .into_dyn()
    .into_pyarray(py)
    .to_owned())
}

#[pyfunction]
fn linear_coefs_from_mesh(data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
    Ok(
        linalg::linear_coefs_from_mesh(py_array_dyn_to_av2!(data)?, true)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned(),
    )
}

#[pyfunction]
fn sig_coefs(data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
    Ok(mcdm::sig_coefs(py_array_dyn_to_av2!(data)?)?
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
}

#[pyfunction]
fn rank_avg(data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
    let result = py_array_dyn_to_av1!(data)?
        .to_ranked_avg()
        .into_dyn()
        .into_pyarray(py)
        .to_owned(); // this doesn't copy
    Ok(result)
}

#[pyfunction]
fn rank_tie(data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<u32>>> {
    let result = py_array_dyn_to_av1!(data)?
        .to_ranked_tie()
        .into_dyn()
        .into_pyarray(py)
        .to_owned(); // this doesn't copy
    Ok(result)
}

#[pyfunction]
fn grid_to_mesh(grid: &PyList, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
    Ok(mcdm::grid_to_mesh(&pylist_to_av1s(grid)?)
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
}

/// test doc 123
#[pyclass]
struct FRM {
    inner: frm::FRM,
}

#[pymethods]
impl FRM {
    #[new]
    #[args(kwargs="**")]
    fn __new__(obj: &PyRawObject, kwargs: Option<&PyDict>) -> PyResult<()> {
        use crate::mcdm::frm::{generate_criterion_weights, Monotonicity};
        use rand::{thread_rng, Rng};

        let mut monotonicity: Option<Monotonicity> = None;
        let mut monotonicities: Option<Vec<Monotonicity>> = None;
        let mut criterion_weights: Option<Vec<f64>> = None;
        let mut granule_cnt: Option<u8> = None;
        let mut granule_cnts: Option<Vec<u8>> = None;
        let mut criterion_cnt: Option<u8> = None;

        if let Some(kwargs) = kwargs {
            for (key, value) in kwargs.iter() {
                let key_str = key
                    .extract::<&PyString>()
                    .map_err(|_| value_error!("Oczekiwano stringów jako kluczy kwargs."))?
                    .to_string()?;
                match key_str.as_ref() {
                    "monotonicity" | "m" => {
                        expect_none!(monotonicity);
                        monotonicity = Some(TryFrom::try_from(value)?);
                    },
                    "monotonicities" | "ms" => {
                        let error_msg = "`monotonicities` to lista string'ów.";
                        expect_none!(monotonicities);
                        monotonicities = Some(
                            value
                                .extract::<&PyList>()
                                .map_err(|_| value_error!(error_msg))?
                                .iter()
                                .map(TryFrom::try_from)
                                .collect::<Result<Vec<_>, _>>()?,
                        )
                    },
                    "criterion_weights" | "cws" => {
                        let error_msg =
                            "`criterion_weights` to lista float'ów, których suma równa się `1.0`.";
                        expect_none!(criterion_weights);
                        criterion_weights = Some(
                            value
                                .extract::<&PyList>()
                                .map_err(|_| value_error!(error_msg))?
                                .iter()
                                .map(|obj| obj.extract::<f64>().map_err(|_| value_error!(error_msg)))
                                .collect::<Result<Vec<_>, _>>()?,
                        )
                    },
                    "granule_cnt" | "gc" => {
                        let error_msg = "`granule_cnt` to liczba całkowita większa od `1`.";
                        expect_none!(granule_cnt);
                        granule_cnt = Some(value.extract::<u8>().map_err(|_| value_error!(error_msg))?)
                    },
                    "granule_cnts" | "gcs" => {
                        let error_msg = "`granule_cnts` to lista liczb całkowitych większych od `1`.";
                        expect_none!(granule_cnts);
                        granule_cnts = Some(
                            value
                                .extract::<&PyList>()
                                .map_err(|_| value_error!(error_msg))?
                                .iter()
                                .map(|obj| obj.extract::<u8>().map_err(|_| value_error!(error_msg)))
                                .collect::<Result<Vec<_>, _>>()?,
                        )
                    },
                    "criterion_cnt" => {
                        let error_msg = "`criterion_cnt` to liczba całkowita większa od `1`.";
                        expect_none!(criterion_cnt);
                        criterion_cnt =
                            Some(value.extract::<u8>().map_err(|_| value_error!(error_msg))?)
                    },
                    _ => {
                        return Err(value_error!(format!(
                            "Nieoczekiwany argument - `{}`.",
                            key_str
                        )));
                    },
                }
            }
        }

        if monotonicity.is_some() && monotonicities.is_some() {
            return Err(value_error!(
                "`monotonicity` i `monotonicities` wykluczają się."
            ));
        }
        if granule_cnt.is_some() && granule_cnts.is_some() {
            return Err(value_error!(
                "`granule_cnt` i `granule_cnts` wykluczają się."
            ));
        }

        let mut rng = thread_rng();

        if criterion_cnt.is_none() {
            // set any inferred value; impl frm::FRM will detect nonequal criterion cnts
            if let Some(v) = monotonicities {
                criterion_cnt = Some(v.len() as u8);
                monotonicities = Some(v);
            }
            if let Some(v) = granule_cnts {
                criterion_cnt = Some(v.len() as u8);
                granule_cnts = Some(v);
            }
            if let Some(v) = criterion_weights {
                criterion_cnt = Some(v.len() as u8);
                criterion_weights = Some(v);
            }

            if criterion_cnt.is_none() {
                criterion_cnt = Some(rng.gen_range(2, 6));
            }
        }

        let criterion_cnt = criterion_cnt.unwrap();

        if monotonicities.is_none() {
            monotonicities = Some(if let Some(monotonicity) = monotonicity {
                vec![monotonicity; criterion_cnt as usize]
            } else {
                let mut ms = Vec::<Monotonicity>::with_capacity(criterion_cnt as usize);
                if let Some(v) = granule_cnts {
                    for &granule_cnt in v.iter() {
                        ms.push(if granule_cnt < 3 {
                            if rng.gen() {
                                Monotonicity::Profit
                            } else {
                                Monotonicity::Cost
                            }
                        } else {
                            Monotonicity::random(&mut rng)
                        });
                    }
                    granule_cnts = Some(v);
                } else {
                    for _ in 0..criterion_cnt {
                        ms.push(Monotonicity::random(&mut rng));
                    }
                }
                ms
            });
        }
        let monotonicities = monotonicities.unwrap();

        if criterion_weights.is_none() {
            criterion_weights = Some(generate_criterion_weights(criterion_cnt as usize, &mut rng));
        }
        let criterion_weights = criterion_weights.unwrap();

        if granule_cnts.is_none() {
            granule_cnts = Some(if let Some(granule_cnt) = granule_cnt {
                vec![granule_cnt; criterion_cnt as usize]
            } else {
                (0..criterion_cnt).map(|_| rng.gen_range(3, 6)).collect()
            });
        }
        let granule_cnts = granule_cnts.unwrap();

        let inner = frm::FRM::new(monotonicities, criterion_weights, granule_cnts)?;

        Ok(obj.init({ FRM { inner } }))
    }

    #[staticmethod]
    #[args(monotonicities = "*")]
    fn from_monotonicities(monotonicities: &PyTuple) -> PyResult<FRM> {
        Ok(FRM {
            inner: frm::FRM::from_monotonicities(
                monotonicities
                    .iter()
                    .map(TryFrom::try_from)
                    .collect::<Result<Vec<_>, _>>()?,
            )?,
        })
    }

    fn rules(&self, py: Python) -> Py<PyArrayDyn<f64>> {
        self.inner
            .rules
            .to_array()
            .into_dyn()
            .into_pyarray(py)
            .to_owned() // this doesn't copy
    }

    fn monotonicities(&self) -> PyResult<Vec<String>> {
        Ok(self
            .inner
            .monotonicities
            .iter()
            .map(|x| x.to_string())
            .collect())
    }

    fn criterion_ws(&self, py: Python) -> Py<PyArrayDyn<f64>> {
        Array1::from(self.inner.criterion_ws.clone())
            .into_dyn()
            .into_pyarray(py)
            .to_owned()
    }

    fn granule_cnts(&self, py: Python) -> Py<PyArrayDyn<u8>> {
        Array1::from(self.inner.granule_cnts.clone())
            .into_dyn()
            .into_pyarray(py)
            .to_owned()
    }

    fn granule_vss(&self, py: Python) -> Vec<Py<PyArrayDyn<f64>>> {
        let granule_vss = &self.inner.granule_vss;
        let mut arrays = Vec::<Py<PyArrayDyn<f64>>>::with_capacity(granule_vss.len());
        for granule_vs in granule_vss.iter() {
            arrays.push(
                Array1::from(granule_vs.clone())
                    .into_dyn()
                    .into_pyarray(py)
                    .to_owned(),
            )
        }
        arrays
    }

    fn shape(&self, py: Python) -> Py<PyArrayDyn<u8>> {
        self.granule_cnts(py)
    }

    fn dim(&self) -> u32 {
        self.inner.dim() as u32
    }

    fn linear_coefs(&self, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(Array1::from(self.inner.lstsq()?.coefs)
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn linear_coefs_nonlinearity(&self, py: Python) -> PyResult<(Py<PyArrayDyn<f64>>, f64)> {
        let (linear_coefs, nonlinearity) = self.inner.linear_coefs_nonlinearity()?;
        Ok((
            Array1::from(linear_coefs)
                .into_dyn()
                .into_pyarray(py)
                .to_owned(),
            nonlinearity,
        ))
    }

    fn nonlinearity(&self) -> PyResult<f64> {
        Ok(self.inner.nonlinearity()?)
    }

    fn invoke_array1(&self, data: &PyArrayDyn<f64>) -> PyResult<f64> {
        Ok(self
            .inner
            .rules
            .invoke_array1(&py_array_dyn_to_av1!(data)?)?)
    }

    fn invoke_array2(&self, data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_array2(&py_array_dyn_to_av2!(data)?)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn invoke_grid2(
        &self,
        xx: &PyArrayDyn<f64>,
        yy: &PyArrayDyn<f64>,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_grid2_array(&py_array_dyn_to_av1!(xx)?, &py_array_dyn_to_av1!(yy)?)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn invoke_grid(&self, grid: &PyList, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_grid(&pylist_to_av1s(grid)?)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn rank_grid_avg(&self, grid: &PyList, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_grid(&pylist_to_av1s(grid)?)?
            .view()
            .to_ranked_avg()
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }
}

#[pyclass]
struct COMET {
    inner: comet::COMET,
}

#[pymethods]
impl COMET {
    #[staticmethod]
    fn from_mesh_y(mesh_y: &PyArrayDyn<f64>) -> PyResult<COMET> {
        if let &[rows, cols] = mesh_y.shape() {
            Ok(COMET {
                inner: comet::COMET::from_mesh_y(&mesh_y.reshape((rows, cols))?.as_array())?,
            })
        } else {
            Err(value_error!(concat!(
                "Macierz powinna być 2-wymiarowa.\n",
                "[[x11, x12, ..., y],\n",
                " [x21, x22, ..., y],\n",
                "               ...]]",
            )))
        }
    }

    #[staticmethod]
    fn from_grid_y(grid: &PyList, y: &PyArrayDyn<f64>) -> PyResult<COMET> {
        Ok(COMET {
            inner: comet::COMET::from_grid_y(&pylist_to_av1s(grid)?, &py_array_dyn_to_av1!(y)?)?,
        })
    }

    fn mej(&self, py: Python) -> Py<PyArrayDyn<f64>> {
        let mej = &self.inner.mej;
        let t = mej.t as usize;
        Array2::from_shape_fn((t, t), |(row, col)| mej.get(row, col))
            .into_dyn()
            .into_pyarray(py)
            .to_owned()
    }

    fn sj(&self, py: Python) -> Py<PyArrayDyn<f64>> {
        Array1::from(self.inner.sj.clone())
            .into_dyn()
            .into_pyarray(py)
            .to_owned()
    }

    fn p(&self, py: Python) -> Py<PyArrayDyn<f64>> {
        Array1::from(self.inner.p.clone())
            .into_dyn()
            .into_pyarray(py)
            .to_owned()
    }

    fn rules(&self, py: Python) -> Py<PyArrayDyn<f64>> {
        self.inner
            .rules
            .to_array()
            .into_dyn()
            .into_pyarray(py)
            .to_owned() // this doesn't copy
    }

    fn granule_cnts(&self, py: Python) -> Py<PyArrayDyn<u8>> {
        Array1::from(self.inner.granule_cnts.clone())
            .into_dyn()
            .into_pyarray(py)
            .to_owned()
    }

    fn shape(&self, py: Python) -> Py<PyArrayDyn<u8>> {
        self.granule_cnts(py)
    }

    fn invoke_array1(&self, data: &PyArrayDyn<f64>) -> PyResult<f64> {
        Ok(self
            .inner
            .rules
            .invoke_array1(&py_array_dyn_to_av1!(data)?)?)
    }

    fn invoke_array2(&self, data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_array2(&py_array_dyn_to_av2!(data)?)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn rank_array2_avg(&self, data: &PyArrayDyn<f64>, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_array2(&py_array_dyn_to_av2!(data)?)?
            .view()
            .to_ranked_avg()
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn invoke_grid2(
        &self,
        xx: &PyArrayDyn<f64>,
        yy: &PyArrayDyn<f64>,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_grid2_array(&py_array_dyn_to_av1!(xx)?, &py_array_dyn_to_av1!(yy)?)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn invoke_grid(&self, grid: &PyList, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_grid(&pylist_to_av1s(grid)?)?
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }

    fn rank_grid_avg(&self, grid: &PyList, py: Python) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(self
            .inner
            .rules
            .invoke_grid(&pylist_to_av1s(grid)?)?
            .view()
            .to_ranked_avg()
            .into_dyn()
            .into_pyarray(py)
            .to_owned())
    }
}

// fixme most of comet is a copy of frm

#[pyclass]
struct TOPSIS {
    inner: topsis::TOPSIS,
}

#[pymethods]
impl TOPSIS {
    #[staticmethod]
    fn invoke_sig_coefs(
        data: &PyArrayDyn<f64>,
        sig_coefs: &PyArrayDyn<f64>,
        criterion_types: &PyList,
        normalization: &PyString,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(topsis::TOPSIS::invoke_sig_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(sig_coefs)?,
            &criterion_types
                .iter()
                .map(TryFrom::try_from)
                .collect::<Result<Vec<_>, _>>()?,
            TryFrom::try_from(normalization)?,
        )?
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }

    #[staticmethod]
    fn invoke_linear_coefs(
        data: &PyArrayDyn<f64>,
        linear_coefs: &PyArrayDyn<f64>,
        normalization: &PyString,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(topsis::TOPSIS::invoke_linear_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(linear_coefs)?,
            TryFrom::try_from(normalization)?,
        )?
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }

    #[staticmethod]
    fn rank_sig_coefs_avg(
        data: &PyArrayDyn<f64>,
        sig_coefs: &PyArrayDyn<f64>,
        criterion_types: &PyList,
        normalization: &PyString,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(topsis::TOPSIS::invoke_sig_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(sig_coefs)?,
            &criterion_types
                .iter()
                .map(TryFrom::try_from)
                .collect::<Result<Vec<_>, _>>()?,
            TryFrom::try_from(normalization)?,
        )?
        .view()
        .to_ranked_avg()
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }

    #[staticmethod]
    fn rank_linear_coefs_avg(
        data: &PyArrayDyn<f64>,
        linear_coefs: &PyArrayDyn<f64>,
        normalization: &PyString,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(topsis::TOPSIS::invoke_linear_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(linear_coefs)?,
            TryFrom::try_from(normalization)?,
        )?
        .view()
        .to_ranked_avg()
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }
}

#[pyclass]
struct AHP {
    inner: ahp::AHP,
}

#[pymethods]
impl AHP {
    #[staticmethod]
    fn invoke_sig_coefs(
        data: &PyArrayDyn<f64>,
        sig_coefs: &PyArrayDyn<f64>,
        criterion_types: &PyList,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(ahp::AHP::invoke_sig_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(sig_coefs)?,
            &criterion_types
                .iter()
                .map(TryFrom::try_from)
                .collect::<Result<Vec<_>, _>>()?,
        )?
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }

    #[staticmethod]
    fn invoke_linear_coefs(
        data: &PyArrayDyn<f64>,
        linear_coefs: &PyArrayDyn<f64>,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(ahp::AHP::invoke_linear_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(linear_coefs)?,
        )?
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }

    #[staticmethod]
    fn rank_sig_coefs_avg(
        data: &PyArrayDyn<f64>,
        sig_coefs: &PyArrayDyn<f64>,
        criterion_types: &PyList,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(ahp::AHP::invoke_sig_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(sig_coefs)?,
            &criterion_types
                .iter()
                .map(TryFrom::try_from)
                .collect::<Result<Vec<_>, _>>()?,
        )?
        .view()
        .to_ranked_avg()
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }

    #[staticmethod]
    fn rank_linear_coefs_avg(
        data: &PyArrayDyn<f64>,
        linear_coefs: &PyArrayDyn<f64>,
        py: Python,
    ) -> PyResult<Py<PyArrayDyn<f64>>> {
        Ok(ahp::AHP::invoke_linear_coefs(
            &py_array_dyn_to_av2!(data)?,
            &py_array_dyn_to_av1!(linear_coefs)?,
        )?
        .view()
        .to_ranked_avg()
        .into_dyn()
        .into_pyarray(py)
        .to_owned())
    }
}

#[pyfunction]
fn spearman(a: &PyArrayDyn<f64>, b: &PyArrayDyn<f64>) -> PyResult<f64> {
    Ok(stat::spearman(
        &py_array_dyn_to_av1!(a)?,
        &py_array_dyn_to_av1!(b)?,
    )?)
}

#[pyfunction]
fn kendall(a: &PyArrayDyn<f64>, b: &PyArrayDyn<f64>) -> PyResult<f64> {
    Ok(stat::kendall(
        &py_array_dyn_to_av1!(a)?,
        &py_array_dyn_to_av1!(b)?,
    )?)
}

#[pyfunction]
fn kruskal(a: &PyArrayDyn<f64>, b: &PyArrayDyn<f64>) -> PyResult<f64> {
    Ok(stat::kruskal(
        &py_array_dyn_to_av1!(a)?,
        &py_array_dyn_to_av1!(b)?,
    )?)
}

#[pyfunction]
fn skk(a: &PyArrayDyn<f64>, b: &PyArrayDyn<f64>) -> PyResult<(f64, f64, f64)> {
    Ok(stat::skk(
        &py_array_dyn_to_av1!(a)?,
        &py_array_dyn_to_av1!(b)?,
    )?)
}

// todo make submodules when new version stabilizes

#[pymodule]
fn mcdm(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_wrapped(wrap_pyfunction!(linear_coefs))?;
    m.add_wrapped(wrap_pyfunction!(linear_coefs_from_mesh))?;

    m.add_wrapped(wrap_pyfunction!(sig_coefs))?;

    m.add_wrapped(wrap_pyfunction!(rank_avg))?;
    m.add_wrapped(wrap_pyfunction!(rank_tie))?;

    m.add_wrapped(wrap_pyfunction!(grid_to_mesh))?;

    m.add_wrapped(wrap_pyfunction!(spearman))?;
    m.add_wrapped(wrap_pyfunction!(kendall))?;
    m.add_wrapped(wrap_pyfunction!(kruskal))?;
    m.add_wrapped(wrap_pyfunction!(skk))?;

    m.add_class::<FRM>()?;
    m.add_class::<COMET>()?;
    m.add_class::<TOPSIS>()?;
    m.add_class::<AHP>()?;
    Ok(())
}
