use nalgebra::{
    base::{dimension::Dynamic, storage::ContiguousStorageMut, DMatrix, Matrix},
    linalg::LU,
};
use ndarray::{Array1, ArrayView1, ArrayView2, Axis};

#[derive(Debug)]
pub enum LAError {
    DegenerateMatrix(String),
    EmptyMatrix(String),
    MismatchedDimension(String),
}

pub struct LstSq {
    pub coefs: Vec<f64>,
    pub x_offset: Vec<f64>,
    pub y_offset: f64,
}

pub fn linear_coefs_from_mesh(
    mesh: ArrayView2<f64>,
    fit_intercept: bool,
) -> Result<Array1<f64>, LAError> {
    if mesh.is_empty() {
        return Err(LAError::EmptyMatrix("Macierz jest pusta.".to_string()));
    }
    let (_, cols) = mesh.dim();
    let (x_view, y_view) = mesh.split_at(Axis(1), cols - 1);
    let y_view = y_view
        .into_dimensionality()
        .expect("Expected `y_view` to have one column.");
    linear_coefs_from_x_y_arrays(&x_view, &y_view, fit_intercept)
}

pub fn linear_coefs_from_x_y_arrays(
    x: &ArrayView2<f64>,
    y: &ArrayView1<f64>,
    fit_intercept: bool,
) -> Result<Array1<f64>, LAError> {
    let (x_rows, x_cols) = x.dim();
    let y_rows = y.dim();
    if x_rows != y_rows {
        return Err(LAError::MismatchedDimension(format!(
            "Nie można obliczyć liniowych współczynników dla x o rozmiarze {}x{} i y o rozmiarze {}.",
            x_rows, x_cols, y_rows
        )));
    }
    let x_slice = x.to_slice().unwrap();
    let y_slice = y.to_slice().unwrap();
    let x_matrix = DMatrix::from_row_slice(x_rows, x_cols, &x_slice);
    let y_matrix = DMatrix::from_column_slice(y_rows, 1, &y_slice);
    Ok(Array1::from(
        lstsq_from_x_y_matrices(x_matrix, y_matrix, fit_intercept)?.coefs,
    ))
}

pub fn lstsq_from_x_y_matrices<T: ContiguousStorageMut<f64, Dynamic, Dynamic>>(
    mut x: Matrix<f64, Dynamic, Dynamic, T>,
    mut y: Matrix<f64, Dynamic, Dynamic, T>,
    fit_intercept: bool,
) -> Result<LstSq, LAError> {
    let (nrows, ncols) = x.shape();
    let (x_offset, y_offset) = if fit_intercept {
        let mut x_offset = Vec::<f64>::with_capacity(ncols);
        for ncol in 0..ncols {
            let mut col = x.column_mut(ncol);
            let col_sum = col.iter().sum::<f64>();
            let col_offset = col_sum / nrows as f64;
            for x_i in col.iter_mut() {
                *x_i -= col_offset;
            }
            x_offset.push(col_offset);
        }

        let y_slice = y.as_mut_slice();
        let y_sum = y_slice.iter().sum::<f64>();
        let y_offset = y_sum / y_slice.len() as f64;
        for y_i in y_slice.iter_mut() {
            *y_i -= y_offset;
        }

        (x_offset, y_offset)
    } else {
        (vec![0.0; ncols], 0.0)
    };
    let inv = LU::new(x.tr_mul(&x))
        .try_inverse()
        .ok_or_else(|| LAError::DegenerateMatrix("Macierz jest nieodwracalna.".to_string()))?;
    let coefs_matrix = inv * x.transpose() * y;
    let coefs_vec: Vec<f64> = coefs_matrix.data.into();
    Ok(LstSq {
        coefs: coefs_vec,
        x_offset,
        y_offset,
    })
}
