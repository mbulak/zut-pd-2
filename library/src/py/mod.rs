use crate::{
    linalg::LAError,
    mcdm::{
        ahp::{self, AHPError},
        comet::COMETError,
        frm::{self, FRMError},
        topsis::{self, TOPSISError},
        EnumParseError, RulesInvokeError,
    },
    stat::StatError,
};
use pyo3::{
    exceptions,
    prelude::*,
    types::{PyAny, PyString},
    PyErr,
};
use std::convert::{From, TryFrom};

macro_rules! value_error {
    ($msg: expr) => {
        PyErr::new::<exceptions::ValueError, _>($msg)
    };
}

impl From<AHPError> for PyErr {
    fn from(err: AHPError) -> Self {
        use crate::mcdm::ahp::AHPError::*;
        value_error!(match err {
            InsufficientCriterions(msg) => msg,
            UnmatchedSize(msg) => msg,
            ZeroCoef(msg) => msg,
        })
    }
}

impl From<COMETError> for PyErr {
    fn from(err: COMETError) -> Self {
        use crate::mcdm::comet::COMETError::*;
        match err {
            DuplicateCO => value_error!("Obiekty charakterystyczne nie mogą się potwarzać."),
            InsufficientCriterionCnt(msg) => value_error!(msg),
            InvalidCOs => value_error!("Nieprawidłowe obiekty charakterystyczne."),
            InvalidGridYPair(msg) => value_error!(msg),
            MissingCO(msg) => value_error!(msg),
            NaN => value_error!("Argumenty nie mogą zawierać wartości NaN."),
        }
    }
}

impl From<EnumParseError> for PyErr {
    fn from(err: EnumParseError) -> Self {
        value_error!(err.0)
    }
}

impl From<FRMError> for PyErr {
    fn from(err: FRMError) -> Self {
        use crate::mcdm::frm::FRMError::*;
        value_error!(match err {
            InsufficientCriterionCnt(msg) => msg,
            InsufficientGranuleCnt(msg) => msg,
            InvalidCriterionWeights(msg) => msg,
            NonEqualCriterionCnt(msg) => msg,
        })
    }
}

impl From<LAError> for PyErr {
    fn from(err: LAError) -> Self {
        use crate::linalg::LAError::*;
        value_error!(match err {
            DegenerateMatrix(msg) => msg,
            EmptyMatrix(msg) => msg,
            MismatchedDimension(msg) => msg,
        })
    }
}

impl From<RulesInvokeError> for PyErr {
    fn from(err: RulesInvokeError) -> Self {
        match err {
            RulesInvokeError::MismatchedDimension(msg) => value_error!(msg),
        }
    }
}

impl From<StatError> for PyErr {
    fn from(err: StatError) -> Self {
        use crate::stat::StatError::*;
        value_error!(match err {
            MismatchedSize(msg) => msg,
        })
    }
}

impl From<TOPSISError> for PyErr {
    fn from(err: TOPSISError) -> Self {
        use crate::mcdm::topsis::TOPSISError::*;
        value_error!(match err {
            InsufficientCriterions(msg) => msg,
            UnmatchedSize(msg) => msg,
            ZeroCoef(msg) => msg,
        })
    }
}

impl TryFrom<&PyAny> for ahp::CriterionType {
    type Error = PyErr;

    fn try_from(value: &PyAny) -> Result<Self, Self::Error> {
        Ok(value
            .extract::<&PyString>()
            .map_err(|_| value_error!("Oczekiwano stringa."))?
            .to_string()?
            .parse::<ahp::CriterionType>()?)
    }
}

impl TryFrom<&PyAny> for frm::Monotonicity {
    type Error = PyErr;

    fn try_from(value: &PyAny) -> Result<Self, Self::Error> {
        Ok(value
            .extract::<&PyString>()
            .map_err(|_| value_error!("Oczekiwano stringa."))?
            .to_string()?
            .parse::<frm::Monotonicity>()?)
    }
}

impl TryFrom<&PyAny> for topsis::CriterionType {
    type Error = PyErr;

    fn try_from(value: &PyAny) -> Result<Self, Self::Error> {
        Ok(value
            .extract::<&PyString>()
            .map_err(|_| value_error!("Oczekiwano string."))?
            .to_string()?
            .parse::<topsis::CriterionType>()?)
    }
}

impl TryFrom<&PyString> for topsis::Normalization {
    type Error = PyErr;

    fn try_from(value: &PyString) -> Result<Self, Self::Error> {
        Ok(value.to_string()?.parse::<topsis::Normalization>()?)
    }
}
