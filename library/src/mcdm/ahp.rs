use crate::mcdm::{self, EnumParseError};
use ndarray::{Array1, ArrayView1, ArrayView2};
use std::str::FromStr;

pub enum CriterionType {
    Profit,
    Cost,
}

impl FromStr for CriterionType {
    type Err = EnumParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "profit" => Ok(CriterionType::Profit),
            "cost" => Ok(CriterionType::Cost),
            _ => Err(EnumParseError(format!(
                "`{}` nie jest poprawną wartością typu kryterium. \
                 Możliwe wartości to: Profit, Cost.",
                s
            ))),
        }
    }
}

pub enum AHPError {
    InsufficientCriterions(String),
    UnmatchedSize(String),
    ZeroCoef(String),
}

pub struct AHP;

impl AHP {
    pub fn invoke_sig_coefs_unchecked(
        data: &ArrayView2<f64>,
        sig_coefs: &ArrayView1<f64>,
        criterion_types: &[CriterionType],
    ) -> Array1<f64> {
        let (alt_cnt, criterion_cnt) = data.dim();

        // will be used to shift data to start from 1.0 to avoid division by 0
        let shift = 1.0
            - data
                .iter()
                .min_by(|a, b| {
                    a.partial_cmp(b)
                        .expect("Expected to compare data elements successfully.")
                })
                .expect("Expected data to have min.");

        let mut pcm = PCM::new(alt_cnt);
        let mut s = Vec::<Vec<f64>>::with_capacity(criterion_cnt);

        // todo optimize
        for k in 0..criterion_cnt {
            match criterion_types[k] {
                CriterionType::Profit => {
                    // @ skrypt akademicki :: 1 :: 4
                    for i in 1..alt_cnt {
                        let x_ik = unsafe { data.uget([i, k]) } + shift;
                        for j in 0..i {
                            let x_jk = unsafe { data.uget([j, k]) } + shift;
                            pcm.set(i, j, x_ik / x_jk);
                        }
                    }
                },
                CriterionType::Cost => {
                    // @ skrypt akademicki :: 1 :: 4
                    for i in 1..alt_cnt {
                        let x_ik = unsafe { data.uget([i, k]) } + shift;
                        for j in 0..i {
                            let x_jk = unsafe { data.uget([j, k]) } + shift;
                            pcm.set(i, j, x_jk / x_ik); // => 1.0 / (x_ik / x_jk)
                        }
                    }
                },
            }

            let mut sum_cols = Vec::<f64>::with_capacity(alt_cnt);
            for j in 0..alt_cnt {
                let mut sum_col = 0f64;
                for i in 0..alt_cnt {
                    sum_col += pcm.get(i, j);
                }
                sum_cols.push(sum_col)
            }

            let mut s_j = Vec::<f64>::with_capacity(alt_cnt);
            for i in 0..alt_cnt {
                let mut sum_row = 0f64;
                for j in 0..alt_cnt {
                    sum_row += pcm.get(i, j) / sum_cols[j];
                }
                s_j.push(sum_row / alt_cnt as f64);
            }
            s.push(s_j);
        }

        // todo s is rotated make it normal orientation

        let mut v = Vec::<f64>::with_capacity(alt_cnt);
        unsafe {
            v.set_len(alt_cnt);
        }
        for i in 0..alt_cnt {
            let mut v_i = 0f64;
            for j in 0..criterion_cnt {
                v_i += s[j][i] * sig_coefs[j];
            }
            v[i] = v_i;
        }
        Array1::from(v)
    }

    pub fn invoke_sig_coefs(
        data: &ArrayView2<f64>,
        sig_coefs: &ArrayView1<f64>,
        criterion_types: &[CriterionType],
    ) -> Result<Array1<f64>, AHPError> {
        let (_, criterion_cnt) = data.dim();

        if criterion_cnt < 2 {
            return Err(AHPError::InsufficientCriterions(format!(
                "Oczekiwano min 2 kryteriów. Otrzymano {}.",
                criterion_cnt
            )));
        }
        if sig_coefs.len() != criterion_cnt {
            return Err(AHPError::UnmatchedSize(format!(
                "Bazując na wymiarach danych oczekiwano {} współczynników istotności. Otrzymano {}.",
                criterion_cnt, sig_coefs.len()
            )));
        }
        if criterion_types.len() != criterion_cnt {
            return Err(AHPError::UnmatchedSize(format!(
                "Bazując na wymiarach danych oczekiwano {} typów kryteriów. Otrzymano {}.",
                criterion_cnt,
                criterion_types.len()
            )));
        }

        if sig_coefs
            .as_slice()
            .unwrap()
            .iter()
            .any(|&x| x < std::f64::EPSILON)
        {
            return Err(AHPError::ZeroCoef(
                "Jeden ze współczynników istotności jest równy zero.".to_string(),
            ));
        }

        Ok(Self::invoke_sig_coefs_unchecked(
            data,
            sig_coefs,
            criterion_types,
        ))
    }

    pub fn invoke_linear_coefs(
        data: &ArrayView2<f64>,
        linear_coefs: &ArrayView1<f64>,
    ) -> Result<Array1<f64>, AHPError> {
        let criterion_cnt = linear_coefs.len();

        let mut criterion_types = Vec::<CriterionType>::with_capacity(criterion_cnt);

        for &coef in linear_coefs.iter() {
            criterion_types.push(if coef >= 0.0 {
                CriterionType::Profit
            } else {
                CriterionType::Cost
            });
        }

        let mut sig_coefs = linear_coefs.to_owned();
        mcdm::linear_to_sig_in_place(&mut sig_coefs);

        Self::invoke_sig_coefs(data, &sig_coefs.view(), &criterion_types)
    }
}

/// Pairwise Comparison Matrix.
/// Optimized for row-first iteration.
/// Performs unsafe initialization, which may result in
/// invalid results for `get` if PCM hasn't been fully `set`.
struct PCM {
    data: Vec<f64>,
}

impl PCM {
    fn new(m: usize) -> Self {
        let capacity = m * (m - 1) / 2;
        let mut data = Vec::<f64>::with_capacity(capacity);
        unsafe {
            data.set_len(capacity);
        }
        Self { data }
    }

    fn get(&self, row: usize, col: usize) -> f64 {
        if row > col {
            self.data[row * (row - 1) / 2 + col]
        } else if row < col {
            1.0 / self.data[col * (col - 1) / 2 + row]
        } else {
            1.0
        }
    }

    fn set(&mut self, row: usize, col: usize, v: f64) {
        if row > col {
            self.data[row * (row - 1) / 2 + col] = v;
        } else if row < col {
            self.data[col * (col - 1) / 2 + row] = 1.0 / v;
        }
    }
}
