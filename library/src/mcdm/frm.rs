use crate::{
    linalg::{self, LAError, LstSq},
    mcdm::{fuzzy_eq, EnumParseError, MultiBaseNum, PrunningRules, Rule, StandardRules, TFN},
};
use nalgebra::{self, base::DMatrix};
use rand::{thread_rng, Rng};
use std::{fmt, str::FromStr};

type Granule = TFN;

#[derive(Clone, Copy, Debug)]
pub enum Monotonicity {
    Profit,
    Cost,
    Nonmonotonic,
}

impl Monotonicity {
    pub fn random<R: Rng + ?Sized>(rng: &mut R) -> Self {
        match rng.gen_range(0, 3) {
            0 => Monotonicity::Profit,
            1 => Monotonicity::Cost,
            _ => Monotonicity::Nonmonotonic,
        }
    }
}

impl fmt::Display for Monotonicity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Monotonicity::Profit => "Profit",
            Monotonicity::Cost => "Cost",
            Monotonicity::Nonmonotonic => "Nonmonotonic",
        })
    }
}

impl FromStr for Monotonicity {
    type Err = EnumParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "profit" | "p" => Ok(Monotonicity::Profit),
            "cost" | "c" => Ok(Monotonicity::Cost),
            "nonmonotonic" | "n" => Ok(Monotonicity::Nonmonotonic),
            _ => Err(EnumParseError(format!(
                "`{}` nie jest poprawną wartością monotoniczności. \
                 Możliwe wartości to: Profit, Cost, Nonmonotonic.",
                s
            ))),
        }
    }
}

pub fn generate_criterion_weights<R: Rng + ?Sized>(criterion_cnt: usize, rng: &mut R) -> Vec<f64> {
    debug_assert!(criterion_cnt >= 2);
    let step_cnt = criterion_cnt + 1;
    let mut steps = Vec::<f64>::with_capacity(step_cnt);
    steps.push(0.0);
    for _ in 1..step_cnt - 1 {
        steps.push(rng.gen());
    }
    steps.push(1.0);
    (&mut steps[1..step_cnt - 1]).sort_unstable_by(|a, b| a.partial_cmp(&b).unwrap());

    let mut ws = Vec::<f64>::with_capacity(criterion_cnt);
    for i in 0..criterion_cnt {
        ws.push(steps[i + 1] - steps[i]);
    }
    ws
}

#[derive(Debug)]
pub enum FRMError {
    InsufficientCriterionCnt(String),
    InsufficientGranuleCnt(String),
    InvalidCriterionWeights(String),
    NonEqualCriterionCnt(String),
}

use crate::mcdm::Rules;

pub struct FRM {
    pub rules: Box<dyn Rules>,
    pub monotonicities: Vec<Monotonicity>,
    pub criterion_ws: Vec<f64>,
    pub granule_cnts: Vec<u8>,
    pub granule_vss: Vec<Vec<f64>>,
}

impl FRM {
    fn new_unchecked<R: Rng + ?Sized>(
        monotonicities: Vec<Monotonicity>,
        criterion_ws: Vec<f64>,
        granule_cnts: Vec<u8>,
        rng: &mut R,
    ) -> Self {
        // fixme shrink_right for min = shrink_left for max etc; shouldn't be like this
        fn rand_shrink_right_sorted<R: Rng + ?Sized>(target: &mut [f64], rng: &mut R) {
            let len = target.len();
            let from = target[0];
            let to = target[len - 1];
            if from < to {
                let shrink_step = (to - from) * 0.75 / len as f64;
                let mut lower = from;
                let upper = to;
                for i in 1..len - 1 {
                    target[i] = rng.gen_range(lower, upper);
                    lower += shrink_step;
                }
                (&mut target[1..len - 1]).sort_unstable_by(|a, b| a.partial_cmp(b).unwrap())
            } else {
                let shrink_step = (from - to) * 0.75 / len as f64;
                let lower = to;
                let mut upper = from;
                for i in 1..len - 1 {
                    target[i] = rng.gen_range(lower, upper);
                    upper -= shrink_step;
                }
                (&mut target[1..len - 1]).sort_unstable_by(|a, b| b.partial_cmp(a).unwrap())
            }
        }
        fn rand_shrink_left_sorted<R: Rng + ?Sized>(target: &mut [f64], rng: &mut R) {
            let len = target.len();
            let from = target[0];
            let to = target[len - 1];
            if from < to {
                let shrink_step = (to - from) * 0.75 / len as f64;
                let lower = from;
                let mut upper = to;
                for i in (1..len - 1).rev() {
                    target[i] = rng.gen_range(lower, upper);
                    upper -= shrink_step;
                }
                (&mut target[1..len - 1]).sort_unstable_by(|a, b| a.partial_cmp(b).unwrap())
            } else {
                let shrink_step = (from - to) * 0.75 / len as f64;
                let mut lower = to;
                let upper = from;
                for i in (1..len - 1).rev() {
                    target[i] = rng.gen_range(lower, upper);
                    lower += shrink_step;
                }
                (&mut target[1..len - 1]).sort_unstable_by(|a, b| b.partial_cmp(a).unwrap())
            }
        }
        fn rand_fill<R: Rng + ?Sized>(vec: &mut Vec<f64>, from: usize, until: usize, rng: &mut R) {
            if rng.gen() {
                rand_shrink_left_sorted(&mut vec[from..until], rng);
            } else {
                rand_shrink_right_sorted(&mut vec[from..until], rng);
            }
        }

        let criterion_cnt = monotonicities.len();
        let mut t = 1usize;
        let mut granule_vss = Vec::<Vec<f64>>::with_capacity(criterion_cnt);
        let mut granuless = Vec::<Vec<Granule>>::with_capacity(criterion_cnt);

        for criterion_id in 0..criterion_cnt {
            let monotonicity = monotonicities[criterion_id];
            let granule_cnt = granule_cnts[criterion_id] as usize;

            t *= granule_cnt;

            let granules = Granule::generate_uniform(0.0, 1.0, granule_cnt as u8);
            granuless.push(granules);

            let mut granule_vs = Vec::<f64>::with_capacity(granule_cnt);
            unsafe {
                granule_vs.set_len(granule_cnt);
            }
            match monotonicity {
                Monotonicity::Profit => {
                    granule_vs[0] = 0.0;
                    granule_vs[granule_cnt - 1] = 1.0;
                    rand_fill(&mut granule_vs, 0, granule_cnt, rng);
                },
                Monotonicity::Cost => {
                    granule_vs[0] = 1.0;
                    granule_vs[granule_cnt - 1] = 0.0;
                    rand_fill(&mut granule_vs, 0, granule_cnt, rng);
                },
                Monotonicity::Nonmonotonic => {
                    // ! granule_cnt min 3
                    let extremum_id: usize = rng.gen_range(1, granule_cnt - 1);
                    let with_max: bool = rng.gen();
                    if with_max {
                        // with max as extremum
                        granule_vs[0] = 0.0;
                        granule_vs[extremum_id] = 1.0;
                        granule_vs[granule_cnt - 1] = 0.0;
                    } else {
                        // with min as extremum
                        granule_vs[0] = 1.0;
                        granule_vs[extremum_id] = 0.0;
                        granule_vs[granule_cnt - 1] = 1.0;
                    }
                    rand_fill(&mut granule_vs, 0, extremum_id + 1, rng);
                    rand_fill(&mut granule_vs, extremum_id, granule_cnt, rng);
                },
            }
            granule_vss.push(granule_vs);
        }
        let t = t;
        let granule_vss = granule_vss;
        let granuless = granuless;

        if t < 30 {
            let mut rules = Vec::<Rule>::with_capacity(t);
            let mut state = MultiBaseNum::new(&granule_cnts);

            for _ in 0..t {
                let mut granules = Vec::<Granule>::with_capacity(criterion_cnt);
                let mut value = 0f64;
                for (i, &e) in state.values.iter().enumerate() {
                    let e = e as usize;
                    let cw = criterion_ws[i];
                    let gv = granule_vss[i][e];
                    value += cw * gv;
                    granules.push(granuless[i][e].clone());
                }
                rules.push(Rule {
                    co: granules,
                    v: value,
                });

                state.inc();
            }

            Self {
                rules: Box::new(StandardRules(rules)),
                monotonicities,
                criterion_ws,
                granule_cnts,
                granule_vss,
            }
        } else {
            let mut vs = Vec::<f64>::with_capacity(t);
            let mut state = MultiBaseNum::new(&granule_cnts);

            for _ in 0..t {
                let mut v = 0f64;
                for (i, &e) in state.values.iter().enumerate() {
                    let e = e as usize;
                    let cw = criterion_ws[i];
                    let gv = granule_vss[i][e];
                    v += cw * gv;
                }
                vs.push(v);
                state.inc();
            }

            Self {
                rules: Box::new(PrunningRules::new(granuless, vs)),
                monotonicities,
                criterion_ws,
                granule_cnts,
                granule_vss,
            }
        }
    }

    pub fn new(
        monotonicities: Vec<Monotonicity>,
        criterion_ws: Vec<f64>,
        granule_cnts: Vec<u8>,
    ) -> Result<Self, FRMError> {
        if monotonicities.len() != criterion_ws.len() {
            return Err(FRMError::NonEqualCriterionCnt(format!(
                "Otrzymano {} monotoniczności i {} wag kryteriów.",
                monotonicities.len(),
                criterion_ws.len()
            )));
        }
        if monotonicities.len() != granule_cnts.len() {
            return Err(FRMError::NonEqualCriterionCnt(format!(
                "Otrzymano {} monotoniczności i {} liczności granuli.",
                monotonicities.len(),
                granule_cnts.len(),
            )));
        }
        if criterion_ws.len() != granule_cnts.len() {
            return Err(FRMError::NonEqualCriterionCnt(format!(
                "Otrzymano {} wag kryteriów i {} liczności granuli.",
                criterion_ws.len(),
                granule_cnts.len(),
            )));
        }

        let criterion_cnt = monotonicities.len();

        if criterion_cnt < 2 {
            return Err(FRMError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 kryteriów. Otrzymano {}.",
                criterion_cnt
            )));
        }

        if !fuzzy_eq(criterion_ws.iter().sum(), 1.0, 1e-8) {
            return Err(FRMError::InvalidCriterionWeights(format!(
                "Wagi kryteriów dają sumę `{}`. Oczekiwano `1.0`.",
                criterion_ws.iter().sum(): f64
            )));
        }

        for i in 0..criterion_cnt {
            use self::Monotonicity::*;
            let granule_cnt = granule_cnts[i];
            match monotonicities[i] {
                Profit => {
                    if granule_cnt < 2 {
                        return Err(FRMError::InsufficientGranuleCnt(format!(
                            "Kryterium typu zysk wymaga min 2 granuli. Otrzymano {}.",
                            granule_cnt
                        )));
                    }
                },
                Cost => {
                    if granule_cnt < 2 {
                        return Err(FRMError::InsufficientGranuleCnt(format!(
                            "Kryterium typu koszt wymaga min 2 granuli. Otrzymano {}.",
                            granule_cnt
                        )));
                    }
                },
                Nonmonotonic => {
                    if granule_cnt < 3 {
                        return Err(FRMError::InsufficientGranuleCnt(format!(
                            "Niemonotoniczne kryterium wymaga min 3 granuli. Otrzymano {}.",
                            granule_cnt
                        )));
                    }
                },
            }
        }

        Ok(Self::new_unchecked(
            monotonicities,
            criterion_ws,
            granule_cnts,
            &mut thread_rng(),
        ))
    }

    pub fn from_monotonicity(monotonicity: Monotonicity) -> Self {
        Self::from_monotonicities(vec![monotonicity; thread_rng().gen_range(2, 6)])
            .expect("Expected a monotonicity repeated [2, 6) times to be valid argument.")
    }

    pub fn from_monotonicities(monotonicities: Vec<Monotonicity>) -> Result<Self, FRMError> {
        let criterion_cnt = monotonicities.len();
        if criterion_cnt < 2 {
            return Err(FRMError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 monotoniczności. Otrzymano {}.",
                criterion_cnt
            )));
        }

        let mut rng = thread_rng();

        let criterion_ws = generate_criterion_weights(criterion_cnt, &mut rng);
        let mut granule_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for _ in 0..criterion_cnt {
            granule_cnts.push(rng.gen_range(3, 6));
        }

        Ok(Self::new_unchecked(
            monotonicities,
            criterion_ws,
            granule_cnts,
            &mut rng,
        ))
    }

    pub fn from_criterion_weights(criterion_weights: Vec<f64>) -> Result<Self, FRMError> {
        let criterion_cnt = criterion_weights.len();
        if criterion_cnt < 2 {
            return Err(FRMError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 wag kryteriów. Otrzymano {}.",
                criterion_cnt
            )));
        }

        let mut rng = thread_rng();

        let mut monotonicities = Vec::<Monotonicity>::with_capacity(criterion_cnt);
        let mut granule_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for _ in 0..criterion_cnt {
            monotonicities.push(Monotonicity::random(&mut rng));
            granule_cnts.push(rng.gen_range(3, 6));
        }

        Ok(Self::new_unchecked(
            monotonicities,
            criterion_weights,
            granule_cnts,
            &mut rng,
        ))
    }

    pub fn from_granule_cnt(granule_cnt: u8) -> Result<Self, FRMError> {
        // `granule_cnt` is checked later
        Self::from_granule_cnts(vec![granule_cnt; thread_rng().gen_range(2, 6)])
    }

    pub fn from_granule_cnts(granule_cnts: Vec<u8>) -> Result<Self, FRMError> {
        let criterion_cnt = granule_cnts.len();
        if criterion_cnt < 2 {
            return Err(FRMError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 liczności granuli. Otrzymano {}.",
                criterion_cnt
            )));
        }

        let mut rng = thread_rng();

        let criterion_weights = generate_criterion_weights(criterion_cnt, &mut rng);
        let mut monotonicities = Vec::<Monotonicity>::with_capacity(criterion_cnt);
        for &granule_cnt in granule_cnts.iter() {
            monotonicities.push(if granule_cnt < 2 {
                return Err(FRMError::InsufficientGranuleCnt(format!(
                    "Minimalna liczba granuli wynosi 2. Otrzymano {}.",
                    granule_cnt
                )));
            } else if granule_cnt < 3 {
                if rng.gen() {
                    Monotonicity::Profit
                } else {
                    Monotonicity::Cost
                }
            } else {
                Monotonicity::random(&mut rng)
            });
        }

        Ok(Self::new_unchecked(
            monotonicities,
            criterion_weights,
            granule_cnts,
            &mut rng,
        ))
    }

    pub fn from_criterion_cnt(criterion_cnt: u8) -> Result<Self, FRMError> {
        // `criterion_cnt` is checked later
        let mut rng = thread_rng();
        let mut gcs = Vec::<u8>::with_capacity(criterion_cnt as usize);
        for _ in 0..criterion_cnt {
            gcs.push(rng.gen_range(3, 6));
        }
        Self::from_granule_cnts(gcs)
    }

    pub fn random() -> Self {
        Self::from_criterion_cnt(thread_rng().gen_range(2, 6))
            .expect("Expected [2, 6) to be valid criterion_cnt range.")
    }

    pub fn dim(&self) -> usize {
        self.monotonicities.len()
    }

    pub fn lstsq(&self) -> Result<LstSq, LAError> {
        let alt_cnt = self.rules.cnt();
        let criterion_cnt = self.rules.dim();

        let mut x = Vec::<f64>::with_capacity(alt_cnt * criterion_cnt);
        unsafe {
            x.set_len(alt_cnt * criterion_cnt);
        }
        let mut y = Vec::<f64>::with_capacity(alt_cnt);
        unsafe {
            y.set_len(alt_cnt);
        }

        let mut alt_id = 0usize;
        self.rules.for_each_ordered(Box::new(|rule| {
            for (criterion_id, cv) in rule.co.iter().enumerate() {
                x[criterion_id * alt_cnt + alt_id] = cv.m;
            }
            y[alt_id] = rule.v;
            alt_id += 1;
        }));
        let x = DMatrix::from_vec(alt_cnt, criterion_cnt, x);
        let y = DMatrix::from_vec(alt_cnt, 1, y);
        linalg::lstsq_from_x_y_matrices(x, y, true)
    }

    pub fn linear_coefs_nonlinearity(&self) -> Result<(Vec<f64>, f64), LAError> {
        let LstSq {
            coefs: linear_coefs,
            x_offset,
            y_offset,
        } = self.lstsq()?;

        let alt_cnt = self.rules.cnt();
        let mut k_sum = 0f64;
        let mut k_min = std::f64::INFINITY;
        let mut k_max = std::f64::NEG_INFINITY;
        self.rules.for_each_ordered(Box::new(|rule| {
            let k_i = rule.v;
            let mut kl_i = 0f64;
            for (criterion_id, cv) in rule.co.iter().enumerate() {
                kl_i += (cv.m - x_offset[criterion_id]) * linear_coefs[criterion_id];
            }
            kl_i += y_offset;
            k_sum += (k_i - kl_i).abs();
            if k_i < k_min {
                k_min = k_i;
            }
            if k_i > k_max {
                k_max = k_i;
            }
        }));

        let nonlinearity = k_sum / (0.5 * alt_cnt as f64 * (k_max - k_min));
        Ok((linear_coefs, nonlinearity))
    }

    pub fn nonlinearity(&self) -> Result<f64, LAError> {
        Ok(self.linear_coefs_nonlinearity()?.1)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq::assert_approx_eq;

    #[test]
    fn test_frm_nonlinearity() {
        let xx1 = TFN::generate_from_points(&mut [0.00, 0.25, 0.50, 0.75, 1.00]);
        let xx2 = TFN::generate_from_points(&mut [0.00, 0.25, 0.50, 0.75, 1.00]);
        let yy = vec![
            0.00, 0.04, 0.11, 0.69, 0.70, 0.02, 0.13, 0.19, 0.84, 0.97, 0.35, 0.79, 0.81, 0.87,
            0.98, 0.65, 0.81, 0.90, 0.91, 0.99, 0.90, 0.94, 0.95, 0.96, 1.00,
        ];

        let mut rules = Vec::<Rule>::with_capacity(25);
        let mut id = 0;
        for x1 in xx1.iter() {
            for x2 in xx2.iter() {
                rules.push(Rule {
                    co: vec![x1.clone(), x2.clone()],
                    v: yy[id],
                });
                id += 1;
            }
        }

        let frm = FRM {
            rules: Box::new(StandardRules(rules)),
            monotonicities: Vec::new(),
            criterion_ws: Vec::new(),
            granule_cnts: Vec::new(),
            granule_vss: Vec::new(),
        };

        assert_approx_eq!(frm.nonlinearity().unwrap(), 0.291104, 0.000001);
    }
}
