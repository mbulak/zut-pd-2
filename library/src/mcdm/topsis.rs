use crate::mcdm::{self, EnumParseError};
use ndarray::{Array1, Array2, ArrayView1, ArrayView2};
use std::str::FromStr;

pub enum CriterionType {
    Profit,
    Cost,
}

impl FromStr for CriterionType {
    type Err = EnumParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "profit" => Ok(CriterionType::Profit),
            "cost" => Ok(CriterionType::Cost),
            _ => Err(EnumParseError(format!(
                "`{}` nie jest poprawną wartością typu kryterium. \
                 Możliwe wartości to: Profit, Cost.",
                s
            ))),
        }
    }
}

pub enum Normalization {
    M1,
    M2,
    M3,
    M4,
}

impl FromStr for Normalization {
    type Err = EnumParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "m1" => Ok(Normalization::M1),
            "m2" => Ok(Normalization::M2),
            "m3" => Ok(Normalization::M3),
            "m4" => Ok(Normalization::M4),
            _ => Err(EnumParseError(format!(
                "`{}` nie jest poprawną wartością normalizacji. \
                 Możliwe wartości to: M1, M2, M3, M4.",
                s
            ))),
        }
    }
}

pub enum TOPSISError {
    InsufficientCriterions(String),
    UnmatchedSize(String),
    ZeroCoef(String),
}

pub struct TOPSIS;

impl TOPSIS {
    pub fn invoke_sig_coefs_unchecked(
        data: &ArrayView2<f64>,
        sig_coefs: &ArrayView1<f64>,
        criterion_types: &[CriterionType],
        normalization: Normalization,
    ) -> Array1<f64> {
        let (alt_cnt, criterion_cnt) = data.dim();

        // min_i => min_j
        // max_i => max_j

        let mut r = unsafe { Array2::<f64>::uninitialized((alt_cnt, criterion_cnt)) };

        // Step 2
        match normalization {
            Normalization::M1 => {
                for j in 0..criterion_cnt {
                    let mut min_j = unsafe { *data.uget([0, j]) }; // criterion_cnt >= 2 => alt_cnt >= 1
                    let mut max_j = min_j;
                    for i in 1..alt_cnt {
                        let x_ij = unsafe { *data.uget([i, j]) };
                        if x_ij < min_j {
                            min_j = x_ij;
                        } else if x_ij > max_j {
                            max_j = x_ij;
                        }
                    }
                    let mm = max_j - min_j;
                    for i in 0..alt_cnt {
                        let x_ij = unsafe { *data.uget([i, j]) };
                        r[[i, j]] = match criterion_types[j] {
                            CriterionType::Profit => (x_ij - min_j) / mm,
                            CriterionType::Cost => (max_j - x_ij) / mm,
                        };
                    }
                }
            },
            Normalization::M2 => {
                for j in 0..criterion_cnt {
                    let mut max_j = unsafe { *data.uget([0, j]) }; // criterion_cnt >= 2 => alt_cnt >= 1
                    for i in 1..alt_cnt {
                        let x_ij = unsafe { *data.uget([i, j]) };
                        if x_ij > max_j {
                            max_j = x_ij;
                        }
                    }
                    for i in 0..alt_cnt {
                        let x_ij = unsafe { *data.uget([i, j]) };
                        r[[i, j]] = match criterion_types[j] {
                            CriterionType::Profit => x_ij / max_j,
                            CriterionType::Cost => 1.0 - (x_ij / max_j),
                        };
                    }
                }
            },
            Normalization::M3 => {
                for j in 0..criterion_cnt {
                    let mut sum_i = 0f64;
                    for i in 0..alt_cnt {
                        sum_i += unsafe { *data.uget([i, j]) };
                    }
                    for i in 0..alt_cnt {
                        let x_ij = unsafe { *data.uget([i, j]) };
                        r[[i, j]] = match criterion_types[j] {
                            CriterionType::Profit => x_ij / sum_i,
                            CriterionType::Cost => 1.0 - (x_ij / sum_i),
                        };
                    }
                }
            },
            Normalization::M4 => {
                for j in 0..criterion_cnt {
                    let mut sum_i = 0f64;
                    for i in 0..alt_cnt {
                        sum_i += unsafe { *data.uget([i, j]) };
                    }
                    let ssum_i = sum_i.sqrt();
                    for i in 0..alt_cnt {
                        let x_ij = unsafe { *data.uget([i, j]) };
                        r[[i, j]] = match criterion_types[j] {
                            CriterionType::Profit => x_ij / ssum_i,
                            CriterionType::Cost => 1.0 - (x_ij / ssum_i),
                        };
                    }
                }
            },
        }

        // Step 3
        for j in 0..criterion_cnt {
            let w_j = unsafe { *sig_coefs.uget(j) }; // criterion_cnt >= 2 & sig_coefs.len() == criterion_cnt
            for i in 0..alt_cnt {
                r[[i, j]] *= w_j;
            }
        }
        let v = r;

        // Step 4
        let mut pis = Vec::<f64>::with_capacity(criterion_cnt);
        unsafe {
            pis.set_len(criterion_cnt);
        }
        let mut nis = Vec::<f64>::with_capacity(criterion_cnt);
        unsafe {
            nis.set_len(criterion_cnt);
        }
        for j in 0..criterion_cnt {
            let mut min_j = unsafe { *v.uget([0, j]) }; // (cos >= 2) => (alt_cnt  > 0)
            let mut max_j = min_j;
            for i in 1..alt_cnt {
                let v_ij = unsafe { *v.uget([i, j]) };
                if v_ij < min_j {
                    min_j = v_ij;
                } else if v_ij > max_j {
                    max_j = v_ij;
                }
            }
            // As the above normalizations convert cost data to profit data,
            // performing the following operations changes the goal from
            // ranking the alternatives best to worst to ranking alternatives
            // worst to best for cost criterions.
            // match criterion_types[j] {
            //     CriterionType::Profit => {
            //         pis[j] = max_j;
            //         nis[j] = min_j;
            //     },
            //     CriterionType::Cost => {
            //         pis[j] = min_j;
            //         nis[j] = max_j;
            //     },
            // }
            // therefore I replace the above with
            pis[j] = max_j;
            nis[j] = min_j;
        }

        // Step 5 + 6
        let mut c = Vec::<f64>::with_capacity(alt_cnt);
        for i in 0..alt_cnt {
            // Step 5
            let mut sum_pis = 0f64;
            let mut sum_nis = 0f64;
            for j in 0..criterion_cnt {
                let pis_j = pis[j];
                let nis_j = nis[j];
                let v_ij = unsafe { *v.uget([i, j]) };
                let diff_pis = v_ij - pis_j;
                let diff_nis = v_ij - nis_j;
                sum_pis += diff_pis * diff_pis;
                sum_nis += diff_nis * diff_nis;
            }
            let d_pis = sum_pis.sqrt();
            let d_nis = sum_nis.sqrt();

            // Step 6
            let c_i = d_nis / (d_nis + d_pis);
            c.push(c_i);
        }

        Array1::from(c)
    }

    /// Values for `data` are assumed to be in [0; 1].
    /// Values for `sig_coefs` are assumed to be in [0; 1] and add up to 1.0.
    pub fn invoke_sig_coefs(
        data: &ArrayView2<f64>,
        sig_coefs: &ArrayView1<f64>,
        criterion_types: &[CriterionType],
        normalization: Normalization,
    ) -> Result<Array1<f64>, TOPSISError> {
        let (_, criterion_cnt) = data.dim();

        if criterion_cnt < 2 {
            return Err(TOPSISError::InsufficientCriterions(format!(
                "Oczekiwano min 2 kryteriów. Otrzymano {}.",
                criterion_cnt
            )));
        }
        if sig_coefs.len() != criterion_cnt {
            return Err(TOPSISError::UnmatchedSize(format!(
                "Bazując na wymiarach danych oczekiwano {} współczynników istotności. Otrzymano {}.",
                criterion_cnt, sig_coefs.len()
            )));
        }
        if criterion_types.len() != criterion_cnt {
            return Err(TOPSISError::UnmatchedSize(format!(
                "Bazując na wymiarach danych oczekiwano {} typów kryteriów. Otrzymano {}.",
                criterion_cnt,
                criterion_types.len()
            )));
        }

        if sig_coefs
            .as_slice()
            .unwrap()
            .iter()
            .any(|&x| x < std::f64::EPSILON)
        {
            return Err(TOPSISError::ZeroCoef(
                "Jeden ze współczynników istotności jest równy zero.".to_string(),
            ));
        }

        Ok(Self::invoke_sig_coefs_unchecked(
            data,
            sig_coefs,
            criterion_types,
            normalization,
        ))
    }

    pub fn invoke_linear_coefs(
        data: &ArrayView2<f64>,
        linear_coefs: &ArrayView1<f64>,
        normalization: Normalization,
    ) -> Result<Array1<f64>, TOPSISError> {
        let criterion_cnt = linear_coefs.len();

        let mut criterion_types = Vec::<CriterionType>::with_capacity(criterion_cnt);

        for &coef in linear_coefs.iter() {
            criterion_types.push(if coef >= 0.0 {
                CriterionType::Profit
            } else {
                CriterionType::Cost
            });
        }

        let mut sig_coefs = linear_coefs.to_owned();
        mcdm::linear_to_sig_in_place(&mut sig_coefs);

        Self::invoke_sig_coefs(data, &sig_coefs.view(), &criterion_types, normalization)
    }
}
