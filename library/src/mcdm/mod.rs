pub mod ahp;
pub mod comet;
pub mod frm;
pub mod topsis;

use crate::linalg::{self, LAError};
use ndarray::{Array, Array1, Array2, ArrayView1, ArrayView2};
use std::{
    cmp::{Ord, Ordering},
    collections::HashSet,
    hash::{Hash, Hasher},
};

#[inline]
pub fn fuzzy_eq(a: f64, b: f64, eps: f64) -> bool {
    (a - b).abs() < eps
}

pub fn sig_coefs(data: ArrayView2<f64>) -> Result<Array1<f64>, LAError> {
    let mut coefs_array = linalg::linear_coefs_from_mesh(data, true)?;
    linear_to_sig_in_place(&mut coefs_array);
    Ok(coefs_array)
}

pub fn linear_to_sig(linear_coefs: &ArrayView1<f64>) -> Array1<f64> {
    let mut sig_coefs = linear_coefs.to_owned();
    linear_to_sig_in_place(&mut sig_coefs);
    sig_coefs
}

pub fn linear_to_sig_in_place(linear_coefs: &mut Array1<f64>) {
    let coefs_slice = linear_coefs
        .as_slice_mut()
        .expect("Expected Array1 data to be continuous.");
    let mut coefs_sum = 0f64;
    for coef in coefs_slice.iter_mut() {
        let coef_abs = coef.abs();
        *coef = coef_abs;
        coefs_sum += coef_abs;
    }
    for coef in coefs_slice.iter_mut() {
        *coef /= coefs_sum;
    }
}

pub trait Rank<TF, TU> {
    fn to_ranked_avg(&self) -> TF;
    fn to_ranked_tie(&self) -> TU;
}

// todo rules mod

impl<'a> Rank<Array1<f64>, Array1<u32>> for ArrayView1<'a, f64> {
    fn to_ranked_avg(&self) -> Array1<f64> {
        let len = self.len();
        if len == 0 {
            return Array1::zeros(0);
        }

        let mut id_data_pairs: Vec<(usize, f64)> = self.iter().cloned().enumerate().collect();
        id_data_pairs.sort_unstable_by(|(_, data1), (_, data2)| data2.partial_cmp(data1).unwrap());

        let mut id_rank_pairs = Vec::<(usize, f64)>::with_capacity(len);
        unsafe {
            id_rank_pairs.set_len(len);
        }
        let (last_id, mut last_data) = id_data_pairs[0];
        id_rank_pairs[0] = (last_id, 1.0);

        let mut last_ne_idx = 0;
        for i in 1..len {
            let (cur_id, cur_data) = id_data_pairs[i];
            let rank_not_avg = (i + 1) as f64;
            id_rank_pairs[i] = (cur_id, rank_not_avg);
            if last_data != cur_data {
                if last_ne_idx < i - 1 {
                    let avg = ((last_ne_idx + 1) as f64 + i as f64) * 0.5;
                    for j in last_ne_idx..i {
                        id_rank_pairs[j].1 = avg;
                    }
                }
                last_ne_idx = i;
                last_data = cur_data;
            }
        }
        if last_ne_idx != len - 1 {
            let avg = ((last_ne_idx + 1) as f64 + len as f64) * 0.5;
            for j in last_ne_idx..len {
                id_rank_pairs[j].1 = avg;
            }
        }

        id_rank_pairs.sort_unstable_by(|(id1, _), (id2, _)| id1.cmp(id2));
        let ranks: Vec<f64> = id_rank_pairs.into_iter().map(|(_, rank)| rank).collect();
        Array1::from(ranks)
    }

    fn to_ranked_tie(&self) -> Array1<u32> {
        let len = self.len();
        if len == 0 {
            return Array1::zeros(0);
        }

        let mut id_data_pairs: Vec<(usize, f64)> = self.iter().cloned().enumerate().collect();
        id_data_pairs.sort_unstable_by(|(_, data1), (_, data2)| data2.partial_cmp(data1).unwrap());

        let mut id_rank_pairs = Vec::<(usize, usize)>::with_capacity(len);
        unsafe {
            id_rank_pairs.set_len(len);
        }
        let (last_id, mut last_data) = id_data_pairs[0];
        id_rank_pairs[0] = (last_id, 1);

        let mut rank = 1;
        for i in 1..len {
            let (cur_id, cur_data) = id_data_pairs[i];
            if last_data != cur_data {
                rank = i + 1;
                last_data = cur_data;
            }
            id_rank_pairs[i] = (cur_id, rank);
        }

        id_rank_pairs.sort_unstable_by(|(id1, _), (id2, _)| id1.cmp(id2));
        let ranks: Vec<u32> = id_rank_pairs
            .into_iter()
            .map(|(_, rank)| rank as u32)
            .collect();
        Array1::from(ranks)
    }
}

pub fn grid_to_mesh(grid: &[ArrayView1<f64>]) -> Array2<f64> {
    let mut result = Vec::<f64>::new();
    let criterion_cnt = grid.len();
    if criterion_cnt == 0 {
        Array2::from_shape_vec((0, 0), result)
            .expect("Expected to construct `Array2` successfully.")
    } else {
        let mut result_rows = 1;
        let mut cv_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for cvs in grid {
            result_rows *= cvs.len();
            cv_cnts.push(cvs.len() as u8);
        }
        let result_size = result_rows * criterion_cnt;

        result.reserve_exact(result_size);

        let mut state = MultiBaseNum::new(&cv_cnts);
        for _ in 0..result_rows {
            for i in 0..criterion_cnt {
                result.push(unsafe { *grid[i].uget(state.values[i] as usize) });
            }
            state.inc();
        }

        Array2::from_shape_vec((result_rows, criterion_cnt), result)
            .expect("Expected to construct `Array2` successfully.")
    }
}

#[derive(Clone)]
struct MultiBaseNum<'a> {
    bases: &'a Vec<u8>,
    values: Vec<u8>,
}

impl<'a> MultiBaseNum<'a> {
    /// No base can equal 0.
    fn new(bases: &'a Vec<u8>) -> Self {
        let values = vec![0u8; bases.len()];
        Self { bases, values }
    }

    fn inc(&mut self) {
        let len = self.bases.len();
        for i in (0..len).rev() {
            let v = self.values[i] + 1;
            if v == self.bases[i] {
                self.values[i] = 0;
            } else {
                self.values[i] = v;
                break;
            }
        }
    }

    fn reset(&mut self) {
        for e in self.values.iter_mut() {
            *e = 0;
        }
    }
}

#[derive(Debug)]
pub enum RulesInvokeError {
    MismatchedDimension(String),
}

#[derive(Debug)]
pub struct EnumParseError(pub String);

#[derive(Clone)]
pub struct TFN {
    a: f64,
    m: f64,
    b: f64,
}

impl TFN {
    /// Assumed:
    /// - cnt >= 2
    pub fn generate_uniform(min: f64, max: f64, cnt: u8) -> Vec<Self> {
        let cnt = cnt as usize;
        let step = (max - min) / (cnt - 1) as f64;
        let mut result = Vec::<Self>::with_capacity(cnt);
        let mut a = min;
        let mut m = min;
        let mut b = min + step;
        for _ in 0..cnt - 1 {
            result.push(TFN { a, m, b });
            a = m;
            m = b;
            b = b + step;
        }
        b = m;
        result.push(TFN { a, m, b });
        result
    }

    /// Assumed:
    /// - points.len() >= 2
    /// - points are sorted
    pub fn generate_from_points(points: &mut [f64]) -> Vec<Self> {
        let cnt = points.len();
        let mut result = Vec::<Self>::with_capacity(cnt);
        let mut a = points[0];
        let mut m = a;
        let mut b = points[1];
        result.push(TFN { a, m, b });
        for i in 2..cnt {
            a = m;
            m = b;
            b = points[i];
            result.push(TFN { a, m, b });
        }
        a = m;
        m = b;
        // b = b;
        result.push(TFN { a, m, b });
        result
    }

    pub fn membership(&self, x: f64) -> f64 {
        if x == self.m {
            1.0
        } else if self.a <= x && x <= self.m {
            (x - self.a) / (self.m - self.a)
        } else if self.m <= x && x <= self.b {
            (self.b - x) / (self.b - self.m)
        } else {
            0.0
        }
    }

    fn au64(&self) -> u64 {
        unsafe { std::mem::transmute(self.a) }
    }

    fn mu64(&self) -> u64 {
        unsafe { std::mem::transmute(self.m) }
    }

    fn bu64(&self) -> u64 {
        unsafe { std::mem::transmute(self.b) }
    }
}

impl Hash for TFN {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.au64().hash(state);
        self.mu64().hash(state);
        self.bu64().hash(state);
    }
}

impl PartialEq for TFN {
    fn eq(&self, other: &Self) -> bool {
        self.au64() == other.au64() && self.mu64() == other.mu64() && self.bu64() == other.bu64()
    }
}

impl Eq for TFN {}

impl PartialOrd for TFN {
    fn partial_cmp(&self, other: &TFN) -> Option<Ordering> {
        self.m.partial_cmp(&other.m)
    }
}

#[derive(Clone)]
pub struct Rule {
    pub co: Vec<TFN>,
    pub v: f64,
}

pub trait Rules {
    fn invoke_slice_unchecked(&self, query: &[f64]) -> f64;

    fn dim(&self) -> usize;

    fn cnt(&self) -> usize;

    fn check_mismatched_dim(&self, target: usize) -> Result<(), RulesInvokeError> {
        if target != self.dim() {
            Err(RulesInvokeError::MismatchedDimension(format!(
                "Wywołano zapytanie {}-wymiarowe na regułach {}-wymiarowych.",
                target,
                self.dim()
            )))
        } else {
            Ok(())
        }
    }

    fn invoke_slice(&self, data: &[f64]) -> Result<f64, RulesInvokeError> {
        self.check_mismatched_dim(data.len())?;
        Ok(self.invoke_slice_unchecked(data))
    }

    fn invoke_array1(&self, data: &ArrayView1<f64>) -> Result<f64, RulesInvokeError> {
        self.invoke_slice(
            data.as_slice()
                .expect("Array1 should be contiguous and in standard order."),
        )
    }

    fn invoke_array2(&self, data: &ArrayView2<f64>) -> Result<Array1<f64>, RulesInvokeError> {
        let (alt_cnt, criterion_cnt) = data.dim();
        self.check_mismatched_dim(criterion_cnt)?;

        let mut buf = Vec::<f64>::with_capacity(alt_cnt);
        unsafe {
            buf.set_len(alt_cnt);
        }

        for i in 0..alt_cnt {
            let row_slice = data
                .row(i)
                .to_slice()
                .expect("Expected data to be in contiguous and in standard order.");
            buf[i] = self.invoke_slice_unchecked(row_slice);
        }
        Ok(Array1::from(buf))
    }

    fn invoke_grid2_slice(
        &self,
        xx: &[f64],
        yy: &[f64],
    ) -> Result<Vec<Vec<f64>>, RulesInvokeError> {
        self.check_mismatched_dim(2)?;
        let cols = xx.len();
        let rows = yy.len();
        let mut zz = Vec::<Vec<f64>>::with_capacity(rows);
        let mut buf = [0.0, 0.0];
        for &y in yy {
            buf[1] = y;
            let mut zz_row = Vec::<f64>::with_capacity(cols);
            for &x in xx {
                buf[0] = x;
                zz_row.push(self.invoke_slice_unchecked(&buf));
            }
            zz.push(zz_row);
        }
        Ok(zz)
    }

    fn invoke_grid2_array(
        &self,
        xx: &ArrayView1<f64>,
        yy: &ArrayView1<f64>,
    ) -> Result<Array2<f64>, RulesInvokeError> {
        self.check_mismatched_dim(2)?;
        let cols = xx.len();
        let rows = yy.len();
        let mut zz = Vec::<f64>::with_capacity(rows * cols);
        let mut buf = [0.0, 0.0];
        for &y in yy {
            buf[1] = y;
            for &x in xx {
                buf[0] = x;
                zz.push(self.invoke_slice_unchecked(&buf));
            }
        }
        Ok(Array::from_shape_vec((rows, cols), zz).unwrap())
    }

    fn invoke_grid(&self, grid: &[ArrayView1<f64>]) -> Result<Array1<f64>, RulesInvokeError> {
        let criterion_cnt = grid.len();
        self.check_mismatched_dim(criterion_cnt)?;

        let mut result_size = 1;
        let mut cv_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for cvs in grid {
            result_size *= cvs.len();
            cv_cnts.push(cvs.len() as u8);
        }

        let mut result = Vec::<f64>::with_capacity(result_size);

        let mut buf = Vec::<f64>::with_capacity(criterion_cnt);
        unsafe {
            buf.set_len(criterion_cnt);
        }

        let mut state = MultiBaseNum::new(&cv_cnts);

        for _ in 0..result_size {
            for i in 0..criterion_cnt {
                buf[i] = unsafe { *grid[i].uget(state.values[i] as usize) };
            }
            result.push(self.invoke_slice_unchecked(&buf));
            state.inc();
        }

        Ok(Array1::from(result))
    }

    fn to_array(&self) -> Array2<f64>;

    fn for_each_ordered<'a>(&self, f: Box<dyn FnMut(&Rule) + 'a>);
}

/// All related implementations assume that rules are ordered ascending by leftmost criterion.
// todo add function `new` and sort rules
pub struct StandardRules(Vec<Rule>);

impl Rules for StandardRules {
    fn invoke_slice_unchecked(&self, query: &[f64]) -> f64 {
        let mut result = 0f64;
        for rule in self.0.iter() {
            let mut membership = 1f64;
            for (idx, granule) in rule.co.iter().enumerate() {
                membership *= granule.membership(query[idx]);
            }
            result += membership * rule.v;
        }
        result
    }

    fn dim(&self) -> usize {
        self.0.first().map(|x| x.co.len()).unwrap_or(0)
    }

    fn cnt(&self) -> usize {
        self.0.len()
    }

    fn to_array(&self) -> Array2<f64> {
        let rows = self.0.len();
        let cols = self.dim() + 1;
        let mut result = Vec::<f64>::with_capacity(rows * cols);
        for rule in self.0.iter() {
            for cv in rule.co.iter() {
                result.push(cv.m);
            }
            result.push(rule.v);
        }
        Array2::from_shape_vec((rows, cols), result).unwrap()
    }

    fn for_each_ordered<'a>(&self, mut f: Box<dyn FnMut(&Rule) + 'a>) {
        for rule in self.0.iter() {
            f(rule);
        }
    }
}

/// Compared to `StandardRules` reduces number of iterations to
/// absolute minimum, however requires setup, which may be inefficient
/// for rules for criterions small number of characteristic values.
/// E.g.:
/// for criterions with shape (10, 10, 10, 10)
/// `StandardRules` - 10 * 10 * 10 * 10 = 1000 iterations
/// `PrunningRules` - 10 _ 2 * 2 * 2 * 2 = 16 iterations
///
/// Assuming that `vs` map to combinations of characteristic values,
/// where leftmost criterion is the most significant.
pub struct PrunningRules {
    cvss: Vec<Vec<TFN>>,
    vs: Vec<f64>,
    steps: Vec<usize>,
}

impl PrunningRules {
    pub fn new(cvss: Vec<Vec<TFN>>, vs: Vec<f64>) -> Self {
        let criterion_cnt = cvss.len();
        let mut step = 1usize;
        let mut steps = Vec::<usize>::with_capacity(criterion_cnt);
        unsafe {
            steps.set_len(criterion_cnt);
        }
        for i in (0..criterion_cnt).rev() {
            steps[i] = step;
            step *= cvss[i].len();
        }
        Self { cvss, vs, steps }
    }
}

impl Rules for PrunningRules {
    fn invoke_slice_unchecked(&self, query: &[f64]) -> f64 {
        let mut cv_idx_membership_pairs = Vec::<(usize, f64)>::with_capacity(query.len() * 2);
        for (i, cvs) in self.cvss.iter().enumerate() {
            let query_i = query[i];
            for (j, cv) in cvs.iter().enumerate() {
                let membership = cv.membership(query_i);
                if membership != 0.0 {
                    cv_idx_membership_pairs.push((j, membership));
                }
            }
            if cv_idx_membership_pairs.len() & 1 != 0 {
                // `std::usize::MAX` is used to mark pair that doesn't exist.
                cv_idx_membership_pairs.push((std::usize::MAX, std::f64::NAN));
            }
        }

        let mut result = 0f64;

        'outer: for state in 0..(1 << query.len()) {
            let mut co_m = 1f64;
            let mut co_idx = 0usize;
            for i in 0..query.len() {
                let idx = if state & 1 << (query.len() - i - 1) == 0 {
                    2 * i
                } else {
                    2 * i + 1
                };
                let (cv_idx, cv_membership) = cv_idx_membership_pairs[idx];
                if cv_idx == std::usize::MAX {
                    // `cv_index_membership_pair` actually does not exist.
                    continue 'outer;
                }
                co_m *= cv_membership;
                co_idx += self.steps[i] * cv_idx;
            }
            result += co_m * self.vs[co_idx];
        }
        result
    }

    fn dim(&self) -> usize {
        self.cvss.len()
    }

    fn cnt(&self) -> usize {
        self.vs.len()
    }

    fn to_array(&self) -> Array2<f64> {
        let rows = self.vs.len();
        let cols = self.dim() + 1;
        let bases: Vec<u8> = self.cvss.iter().map(|cvs| cvs.len() as u8).collect();
        let mut state = MultiBaseNum::new(&bases);
        let mut result = Vec::<f64>::with_capacity(rows * cols);
        for co_id in 0..self.vs.len() {
            for (i, &e) in state.values.iter().enumerate() {
                let e = e as usize;
                let cv = &self.cvss[i][e];
                result.push(cv.m);
            }
            result.push(self.vs[co_id]);
            state.inc();
        }
        Array2::from_shape_vec((rows, cols), result).unwrap()
    }

    fn for_each_ordered<'a>(&self, mut f: Box<dyn FnMut(&Rule) + 'a>) {
        let criterion_cnt = self.cvss.len();
        let t = self.vs.len();
        let bases: Vec<u8> = self.cvss.iter().map(|cvs| cvs.len() as u8).collect();
        let mut state = MultiBaseNum::new(&bases);
        let mut rule = Rule {
            co: Vec::with_capacity(criterion_cnt),
            v: 0f64,
        };
        for co_id in 0..t {
            rule.co.clear();
            for (i, &e) in state.values.iter().enumerate() {
                let e = e as usize;
                let cv = self.cvss[i][e].clone();
                rule.co.push(cv);
            }
            rule.v = self.vs[co_id];
            f(&rule);
            state.inc();
        }
    }
}

impl From<&PrunningRules> for StandardRules {
    fn from(prunning_rules: &PrunningRules) -> Self {
        let criterion_cnt = prunning_rules.cvss.len();
        let t = prunning_rules.vs.len();
        let bases: Vec<u8> = prunning_rules
            .cvss
            .iter()
            .map(|cvs| cvs.len() as u8)
            .collect();
        let mut state = MultiBaseNum::new(&bases);
        let mut rules = Vec::<Rule>::with_capacity(t);
        for co_id in 0..t {
            let mut co = Vec::<TFN>::with_capacity(criterion_cnt);
            for (i, &e) in state.values.iter().enumerate() {
                let e = e as usize;
                let cv = prunning_rules.cvss[i][e].clone();
                co.push(cv);
            }
            rules.push(Rule {
                co,
                v: prunning_rules.vs[co_id],
            });
            state.inc();
        }
        StandardRules(rules)
    }
}

impl From<&StandardRules> for PrunningRules {
    fn from(standard_rules: &StandardRules) -> Self {
        let mut set = HashSet::<TFN>::new();
        let criterion_cnt = standard_rules.dim();
        let t = standard_rules.0.len();

        let mut step = 1usize;
        let mut steps = Vec::<usize>::with_capacity(criterion_cnt);
        unsafe {
            steps.set_len(criterion_cnt);
        }
        let mut cvss = Vec::<Vec<TFN>>::with_capacity(criterion_cnt);
        let mut vs = Vec::<f64>::with_capacity(t);

        for i in (0..criterion_cnt).rev() {
            steps[i] = step;
            let mut j = 0usize;
            while j < t && set.insert(standard_rules.0[j].co[i].clone()) {
                j += step;
            }
            let mut cvs = set.drain().collect::<Vec<TFN>>();
            cvs.sort_unstable_by(|a, b| a.partial_cmp(b).expect("Expected TFN to be comparable."));
            step *= cvs.len();
            cvss.push(cvs);
        }
        cvss.reverse();

        for co_id in 0..t {
            vs.push(standard_rules.0[co_id].v);
        }

        Self { cvss, vs, steps }
    }
}

#[cfg(test)]
mod tests {
    use crate::mcdm::{frm::FRM, PrunningRules, Rule, Rules, StandardRules};
    use ::test::Bencher;
    use rand::{thread_rng, Rng};

    fn gen_rules_pair(granule_cnts: Vec<u8>) -> (StandardRules, PrunningRules) {
        let frm = FRM::from_granule_cnts(granule_cnts).unwrap();
        let mut rules_raw = Vec::<Rule>::with_capacity(frm.rules.cnt());
        frm.rules
            .for_each_ordered(Box::new(|rule| rules_raw.push(rule.clone())));
        let standard_rules = StandardRules(rules_raw);
        let prunning_rules = (&standard_rules).into();
        (standard_rules, prunning_rules)
    }

    #[test]
    fn test_srules_prules_1() {
        let (srules, prules) = gen_rules_pair(vec![5, 5, 5]);

        let dim = prules.dim();

        let mut rng = thread_rng();

        let mut vec = Vec::<f64>::with_capacity(dim);
        for _ in 0..10000 {
            vec.clear();
            for _ in 0..dim {
                vec.push(rng.gen());
            }
            assert_eq!(
                srules.invoke_slice(vec.as_slice()).unwrap(),
                prules.invoke_slice(vec.as_slice()).unwrap()
            );
        }
    }

    #[bench]
    fn bench_srules_3_3(b: &mut Bencher) {
        let (rules, _) = gen_rules_pair(vec![3, 3]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_prules_3_3(b: &mut Bencher) {
        let (_, rules) = gen_rules_pair(vec![3, 3]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_srules_5_5_5(b: &mut Bencher) {
        let (rules, _) = gen_rules_pair(vec![5, 5, 5]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_prules_5_5_5(b: &mut Bencher) {
        let (_, rules) = gen_rules_pair(vec![5, 5, 5]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_srules_10_10(b: &mut Bencher) {
        let (rules, _) = gen_rules_pair(vec![10, 10]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_prules_10_10(b: &mut Bencher) {
        let (_, rules) = gen_rules_pair(vec![10, 10]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_srules_10_10_10(b: &mut Bencher) {
        let (rules, _) = gen_rules_pair(vec![10, 10, 10]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }

    #[bench]
    fn bench_prules_10_10_10(b: &mut Bencher) {
        let (_, rules) = gen_rules_pair(vec![10, 10, 10]);
        let mut rng = thread_rng();
        let mut vec = Vec::<f64>::with_capacity(rules.dim());

        b.iter(|| {
            vec.clear();
            for _ in 0..rules.dim() {
                vec.push(rng.gen());
            }
            rules.invoke_slice(vec.as_slice()).unwrap()
        });
    }
}
