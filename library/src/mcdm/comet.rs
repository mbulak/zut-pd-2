use crate::mcdm::{MultiBaseNum, Rule, StandardRules, TFN};
use ndarray::{prelude::ArrayView1, ArrayView2};
use std::{
    cmp::{Ordering, PartialOrd},
    collections::HashSet,
    hash::{Hash, Hasher},
    mem::transmute,
};

type CV = TFN;

#[derive(Debug, Copy, Clone, PartialOrd)]
pub struct Num64(f64);

impl Num64 {
    pub fn new(v: f64) -> Option<Self> {
        if v.is_nan() {
            None
        } else {
            Some(Self(v))
        }
    }

    fn new_unsafe(v: f64) -> Self {
        Self(v)
    }

    fn key(&self) -> u64 {
        unsafe { transmute(self.0) }
    }
}

impl Hash for Num64 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.key().hash(state);
    }
}

impl PartialEq for Num64 {
    fn eq(&self, other: &Num64) -> bool {
        self.key() == other.key()
    }
}

impl Eq for Num64 {}

impl Ord for Num64 {
    fn cmp(&self, other: &Num64) -> Ordering {
        self.0.partial_cmp(&other.0).unwrap()
    }
}

/// Optimized for row-first iteration.
pub struct MEJ {
    data: Vec<f64>,
    pub t: u32,
}

impl MEJ {
    pub fn new(t: usize) -> Self {
        Self {
            data: vec![0f64; t * (t - 1) / 2],
            t: (t as u32),
        }
    }

    pub fn get(&self, row: usize, col: usize) -> f64 {
        if row > col {
            self.data[row * (row - 1) / 2 + col]
        } else if row < col {
            1.0 - self.data[col * (col - 1) / 2 + row]
        } else {
            0.5
        }
    }

    pub fn set(&mut self, row: usize, col: usize, v: f64) {
        if row > col {
            self.data[row * (row - 1) / 2 + col] = v;
        } else if row < col {
            self.data[col * (col - 1) / 2 + row] = 1.0 - v;
        }
    }

    pub fn set_cmp(&mut self, row: usize, row_v: f64, col: usize, col_v: f64) {
        self.set(
            row,
            col,
            if row_v < col_v {
                0.0
            } else if row_v == col_v {
                0.5
            } else {
                1.0
            },
        );
    }

    pub fn set_ord(&mut self, row: usize, col: usize, ord: Ordering) {
        self.set(row, col, match ord {
            Ordering::Less => 0.0,
            Ordering::Equal => 0.5,
            Ordering::Greater => 1.0,
        })
    }
}

pub enum COMETError {
    DuplicateCO,
    InsufficientCriterionCnt(String),
    InvalidCOs,
    InvalidGridYPair(String),
    MissingCO(String),
    NaN,
}

pub struct COMET {
    pub mej: MEJ,
    pub sj: Vec<f64>,
    pub p: Vec<f64>,
    pub rules: StandardRules,
    pub granule_cnts: Vec<u8>,
}

impl COMET {
    fn cvs_raw_to_typed(cvs: &mut [f64]) -> Result<Vec<CV>, COMETError> {
        let len = cvs.len();
        if len < 2 {
            return Err(COMETError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 wartości charakterystycznych dla kryterium. Otrzymano {}.",
                cvs.len()
            )));
        }
        cvs.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
        let mut cvs_typed = Vec::<CV>::new();
        cvs_typed.push(CV {
            a: cvs[0],
            m: cvs[0],
            b: cvs[1],
        });
        for i in 1..len - 1 {
            cvs_typed.push(CV {
                a: cvs[i - 1],
                m: cvs[i],
                b: cvs[i + 1],
            });
        }
        cvs_typed.push(CV {
            a: cvs[len - 2],
            m: cvs[len - 1],
            b: cvs[len - 1],
        });
        Ok(cvs_typed)
    }

    fn cvss_raw_to_typed(cvss: &[HashSet<Num64>]) -> Result<Vec<Vec<CV>>, COMETError> {
        let mut cvss_typed = Vec::<Vec<CV>>::new();
        for cvs in cvss.iter() {
            let mut cvs: Vec<f64> = cvs.iter().map(|x| x.0).collect();
            let cvs_typed = Self::cvs_raw_to_typed(&mut cvs)?;
            cvss_typed.push(cvs_typed);
        }
        Ok(cvss_typed)
    }

    fn mej_to_sj(mej: &MEJ) -> Vec<f64> {
        let t = mej.t as usize;
        let mut sj = Vec::<f64>::with_capacity(t);
        for i in 0..t {
            let mut sum = 0.0;
            for j in 0..t {
                sum += mej.get(i, j);
            }
            sj.push(sum);
        }
        sj
    }

    fn sj_to_p(sj: &Vec<f64>) -> Vec<f64> {
        let mut pairs: Vec<(usize, f64)> = sj.iter().cloned().enumerate().collect();
        pairs.sort_unstable_by(|a, b| b.1.partial_cmp(&a.1).unwrap());

        let first = pairs.first().unwrap().1;
        let mut last_value: f64 = first;
        let mut k = 1;
        for &(_, value) in pairs.iter() {
            if value != last_value {
                k += 1;
                last_value = value;
            }
        }

        let delta = 1.0 / (k - 1) as f64;

        let mut last_value = first;
        let mut new_value = 1f64;

        let mut p = vec![1f64; pairs.len()];

        for &(i, value) in pairs.iter() {
            if value != last_value {
                new_value -= delta;
                last_value = value;
            }
            p[i] = new_value;
        }
        p
    }

    fn cos_mej_shape_to_self(cos: Vec<Vec<CV>>, mej: MEJ, shape: Vec<u8>) -> Self {
        let sj = Self::mej_to_sj(&mej);
        let p = Self::sj_to_p(&sj);
        let mut rules = Vec::<Rule>::with_capacity(cos.len());

        for (i, co) in cos.into_iter().enumerate() {
            rules.push(Rule { co, v: p[i] });
        }
        let rules = StandardRules(rules);
        Self {
            mej,
            sj,
            p,
            rules,
            granule_cnts: shape,
        }
    }

    pub fn from_comparator<F: Fn(&[f64], &[f64]) -> Ordering>(
        cvss: Vec<HashSet<Num64>>,
        cmp: F,
    ) -> Result<Self, COMETError> {
        let cvss_typed = Self::cvss_raw_to_typed(&cvss)?;
        let criterion_cnt = cvss_typed.len();

        let mut t = 1usize;
        let mut cv_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for cvs in cvss_typed.iter() {
            t *= cvs.len();
            cv_cnts.push(cvs.len() as u8);
        }
        let t = t;
        let cv_cnts = cv_cnts;

        let mut mej = MEJ::new(t);

        let mut a = Vec::<CV>::with_capacity(criterion_cnt);
        unsafe {
            a.set_len(criterion_cnt);
        }
        let mut a_raw = Vec::<f64>::with_capacity(criterion_cnt);
        unsafe {
            a_raw.set_len(criterion_cnt);
        }
        let mut a_idx = 0usize;
        let mut a_state = MultiBaseNum::new(&cv_cnts);

        let mut cos = Vec::<Vec<CV>>::with_capacity(t);
        let mut cos_raw = Vec::<Vec<f64>>::with_capacity(t);

        while a_idx < t {
            for (i, &e) in a_state.values.iter().enumerate() {
                let cv = cvss_typed[i][e as usize].clone();
                a_raw[i] = cv.m;
                a[i] = cv;
            }
            cos.push(a.clone());
            cos_raw.push(a_raw.clone());

            for b_idx in 0..a_idx {
                mej.set_ord(
                    a_idx,
                    b_idx,
                    cmp(a_raw.as_slice(), cos_raw[b_idx].as_slice()),
                );
            }

            a_idx += 1;
            a_state.inc();
        }

        Ok(Self::cos_mej_shape_to_self(cos, mej, cv_cnts))
    }

    /// Not checking for duplicates in `grid[?]`.
    pub fn from_grid_y(grid: &[ArrayView1<f64>], y: &ArrayView1<f64>) -> Result<Self, COMETError> {
        // fixme if grid[?] is not sorted the produced rules will not be sorted
        // throw exception?
        let criterion_cnt = grid.len();
        if criterion_cnt < 2 {
            return Err(COMETError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 kryteriów. Otrzymano {}.",
                criterion_cnt
            )));
        }

        let mut t = 1usize;
        let mut cvss = Vec::<Vec<CV>>::with_capacity(criterion_cnt);
        let mut cv_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for cvs in grid {
            t *= cvs.len();
            cvss.push(Self::cvs_raw_to_typed(&mut cvs.to_vec())?);
            cv_cnts.push(cvs.len() as u8);
        }
        let t = t;
        let cv_cnts = cv_cnts;

        if y.len() != t {
            return Err(COMETError::InvalidGridYPair(format!(
                "Na podstawie `grid` oczekiwano, że `y` bedzie miało długość `{}`. \
                 Rzeczywista długość - `{}`.",
                t,
                y.len()
            )));
        }

        let mut cos = Vec::<Vec<CV>>::with_capacity(t);
        let mut mej = MEJ::new(t);
        let mut state = MultiBaseNum::new(&cv_cnts);
        let mut a = Vec::<CV>::with_capacity(criterion_cnt);
        unsafe {
            a.set_len(criterion_cnt);
        }

        for a_idx in 0..t {
            for (i, &e) in state.values.iter().enumerate() {
                a[i] = cvss[i][e as usize].clone();
            }
            cos.push(a.clone());

            let a_v = unsafe { *y.uget(a_idx) };
            for b_idx in 0..a_idx {
                let b_v = unsafe { *y.uget(b_idx) };
                mej.set_cmp(a_idx, a_v, b_idx, b_v);
            }

            state.inc();
        }

        Ok(Self::cos_mej_shape_to_self(cos, mej, cv_cnts))
    }

    /// # Input format:
    ///
    /// [[cv, cv, cv, ..., v],
    ///  [cv, cv, cv, ..., v],
    ///  ...                                   
    ///  [cv, cv, cv, ..., v]]
    ///
    ///  cv - characteristic value
    ///  v -  value that combinations of cvs will be compared against
    pub fn from_mesh_y(mesh_y: &ArrayView2<f64>) -> Result<Self, COMETError> {
        let nrows = mesh_y.nrows();
        let ncols = mesh_y.ncols();
        let criterion_cnt = ncols - 1;

        if criterion_cnt < 2 {
            return Err(COMETError::InsufficientCriterionCnt(format!(
                "Oczekiwano min 2 kryteriów. Otrzymano {}.",
                criterion_cnt
            )));
        }

        let mut cvss_raw = Vec::<HashSet<Num64>>::new();
        for _ in 0..criterion_cnt {
            cvss_raw.push(HashSet::new());
        }

        let mut cvs_v_pairs = Vec::<(Vec<f64>, f64)>::with_capacity(nrows);

        for i in 0..nrows {
            let row = mesh_y.row(i);
            let mut cvs = Vec::<f64>::with_capacity(criterion_cnt);
            for j in 0..criterion_cnt {
                let cell = *row.get(j).unwrap();
                if cell.is_nan() {
                    return Err(COMETError::NaN);
                }
                cvss_raw[j].insert(Num64::new_unsafe(cell));
                cvs.push(cell);
            }
            let v = *row.get(criterion_cnt).unwrap();
            if v.is_nan() {
                return Err(COMETError::NaN);
            }
            cvs_v_pairs.push((cvs, v));
        }

        let cvss_typed = Self::cvss_raw_to_typed(&cvss_raw)?;

        let mut t = 1usize;
        let mut cv_cnts = Vec::<u8>::with_capacity(criterion_cnt);
        for cvs in cvss_typed.iter() {
            t *= cvs.len();
            cv_cnts.push(cvs.len() as u8);
        }
        let t = t;
        let cv_cnts = cv_cnts;

        let mut mej = MEJ::new(t);

        if nrows != t {
            return Err(COMETError::MissingCO(format!(
                "Oczekiwano {} obiektów charakterystycznych. Otrzymano {}.",
                t, nrows
            )));
        }

        let mut duplicate_detected = false;
        cvs_v_pairs.sort_unstable_by(|(v1, _), (v2, _)| {
            for i in 0..criterion_cnt {
                match v1[i].partial_cmp(&v2[i]).unwrap() {
                    Ordering::Less => return Ordering::Less,
                    Ordering::Greater => return Ordering::Greater,
                    Ordering::Equal => {},
                }
            }
            duplicate_detected = true;
            Ordering::Equal
        });
        if duplicate_detected {
            return Err(COMETError::DuplicateCO);
        }

        let sorted_cvs_v_pairs = cvs_v_pairs;

        let mut a = Vec::<CV>::with_capacity(criterion_cnt);
        unsafe {
            a.set_len(criterion_cnt);
        }
        let mut a_idx = 0usize; // not 1
        let mut a_state = MultiBaseNum::new(&cv_cnts);

        let mut cos = Vec::<Vec<CV>>::with_capacity(t);

        while a_idx < t {
            let (pair_cvs, pair_v) = &sorted_cvs_v_pairs[a_idx];

            for (i, &e) in a_state.values.iter().enumerate() {
                let e = e as usize;
                let cv = cvss_typed[i][e].clone();
                if (pair_cvs[i] - cv.m).abs() > std::f64::EPSILON {
                    return Err(COMETError::InvalidCOs);
                }
                a[i] = cv;
            }
            cos.push(a.clone());

            for b_idx in 0..a_idx {
                let a_v = *pair_v;
                let b_v = sorted_cvs_v_pairs[b_idx].1;
                mej.set_cmp(a_idx, a_v, b_idx, b_v);
            }

            a_idx += 1;
            a_state.inc();
        }

        Ok(Self::cos_mej_shape_to_self(cos, mej, cv_cnts))
    }
}
