use ndarray::ArrayView1;

type StatResult<T> = Result<T, StatError>;

#[derive(Debug)]
pub enum StatError {
    MismatchedSize(String),
}

fn check_sizes(a: usize, b: usize) -> StatResult<()> {
    if a == b {
        Ok(())
    } else {
        return Err(StatError::MismatchedSize(format!(
            "Oczekiwano tablic równej długości. Otrzymano `{}` i `{}`.",
            a, b,
        )));
    }
}

pub fn spearman(a: &ArrayView1<f64>, b: &ArrayView1<f64>) -> StatResult<f64> {
    check_sizes(a.len(), b.len())?;
    let n = a.len();

    let sum: f64 = a
        .iter()
        .zip(b.iter())
        .map(|(&a, &b)| {
            let d = a - b;
            d * d
        })
        .sum();

    let numerator = 6.0 * sum;
    let denominator = (n * (n * n - 1)) as f64;
    let result = 1.0 - numerator / denominator;
    Ok(result)
}

pub fn kendall(a: &ArrayView1<f64>, b: &ArrayView1<f64>) -> StatResult<f64> {
    check_sizes(a.len(), b.len())?;
    let n = a.len();

    let mut pairs = a
        .iter()
        .cloned()
        .zip(b.iter().cloned())
        .collect::<Vec<(f64, f64)>>();
    pairs.sort_unstable_by(|&(a, _), &(b, _)| a.partial_cmp(&b).unwrap());

    let mut n_s = 0;
    let mut n_d = 0;

    for i in 0..n {
        let (_, b_i) = pairs[i];
        for j in i + 1..n {
            let (_, b_j) = pairs[j];
            if b_i < b_j {
                n_s += 1;
            } else if b_i > b_j {
                n_d += 1;
            }
        }
    }

    let numerator = (n_s - n_d) as f64;
    let denominator = (n * (n - 1)) as f64;
    let result = 2.0 * numerator / denominator;
    Ok(result)
}

pub fn kruskal(a: &ArrayView1<f64>, b: &ArrayView1<f64>) -> StatResult<f64> {
    check_sizes(a.len(), b.len())?;
    let n = a.len();

    let mut pairs = a
        .iter()
        .cloned()
        .zip(b.iter().cloned())
        .collect::<Vec<(f64, f64)>>();
    pairs.sort_unstable_by(|&(a, _), &(b, _)| a.partial_cmp(&b).unwrap());

    let mut n_s = 0;
    let mut n_d = 0;

    for i in 0..n {
        let (_, b_i) = pairs[i];
        for j in i + 1..n {
            let (_, b_j) = pairs[j];
            if b_i < b_j {
                n_s += 1;
            } else if b_i > b_j {
                n_d += 1;
            }
        }
    }

    let numerator = (n_s - n_d) as f64;
    let denominator = (n_s + n_d) as f64;
    let result = numerator / denominator;
    Ok(result)
}

pub fn skk(a: &ArrayView1<f64>, b: &ArrayView1<f64>) -> StatResult<(f64, f64, f64)> {
    check_sizes(a.len(), b.len())?;
    let n = a.len();

    let mut pairs = a
        .iter()
        .cloned()
        .zip(b.iter().cloned())
        .collect::<Vec<(f64, f64)>>();
    pairs.sort_unstable_by(|&(a, _), &(b, _)| a.partial_cmp(&b).unwrap());

    let mut sum_d2 = 0f64;
    let mut n_s = 0;
    let mut n_d = 0;

    for i in 0..n {
        let (a_i, b_i) = pairs[i];
        let d = a_i - b_i;
        sum_d2 += d * d;

        for j in i + 1..n {
            let (_, b_j) = pairs[j];
            if b_i < b_j {
                n_s += 1;
            } else if b_i > b_j {
                n_d += 1;
            }
        }
    }

    let spearman = 1.0 - 6.0 * sum_d2 / (n * (n * n - 1)) as f64;
    let kendall = 2.0 * (n_s - n_d) as f64 / (n * (n - 1)) as f64;
    let kruskal = (n_s - n_d) as f64 / (n_s + n_d) as f64;
    Ok((spearman, kendall, kruskal))
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq::assert_approx_eq;
    use ndarray::Array1;
    use rand::{thread_rng, Rng};

    #[test]
    fn test_fkk_1() {
        let mut rng = thread_rng();

        let n = 100;

        let mut a = Vec::<f64>::with_capacity(n);
        let mut b = Vec::<f64>::with_capacity(n);

        for _ in 0..n {
            a.push(rng.gen_range(1.0, 101.0));
            b.push(rng.gen_range(1.0, 101.0));
        }

        let a = Array1::from(a);
        let b = Array1::from(b);

        let spearman1 = spearman(&a.view(), &b.view()).unwrap();
        let kendall1 = kendall(&a.view(), &b.view()).unwrap();
        let kruskal1 = kruskal(&a.view(), &b.view()).unwrap();

        let (spearman2, kendall2, kruskal2) = skk(&a.view(), &b.view()).unwrap();

        assert_approx_eq!(spearman1, spearman2);
        assert_approx_eq!(kendall1, kendall2);
        assert_approx_eq!(kruskal1, kruskal2);
    }
}
