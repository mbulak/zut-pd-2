Wymagania:
- zainstalowany i skonfigurowany rustup, rust, cargo zgodanie z https://www.rust-lang.org/tools/install
- python 3.8.0 64-bit

Instalacja Rust nightly:
- `rustup default nghtly-2019-11-13`

Instalacja Matirun, używanego do tworzenia paczek dla Pythona:
- `cargo install --version 0.7.7 --force maturin`

--------------------------------------------------------------------------

Wymagane jest aby folder z zainstalowanym maturin (%USERPROFILE%\.cargo\bin) był dostępny w zmiennej środowiskowej PATH.

Aby skompilować bibliotekę i stworzyć paczkę dla Pythona należy przejść do folderu 'library' i odpalić:
- `maturin build --cargo-extra-args="--release"`

--------------------------------------------------------------------------

Wymagany jest pip lub coś innego co będzie mogło instalować paczki w formacie .whl.

Aby zainstalować paczkę należy przejść do folderu library\target\wheels i odpalić:
- `pip install --force-reinstall .\mcdm-0.2.0-cp38-none-win_amd64.whl`

Od teraz można uruchamiać skrypty w folderze scripts.
